package api

import (
	"context"
	"net/http"

	"gitlab.com/vl4deee11/ipx/lib/ipxlang"
	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/ch"
)

func SearchGETH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	query := r.URL.Query().Get("q")
	if query == "" {
		resp.BadRequest()
		return
	}

	limit := getLimit(r)
	offset := getOffset(r)

	compiledQ, err := ipxlang.Compiler.Compile(resp.Ctx, query)
	if err != nil {
		resp.BadRequest()
		return
	}

	results, err := ch.Repo.Search(resp.Ctx, compiledQ, limit, offset)
	if err != nil {
		resp.ISE(err)
		return
	}

	resp.OK(results)
}
