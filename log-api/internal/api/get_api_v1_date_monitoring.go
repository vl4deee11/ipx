package api

import (
	"context"
	"net/http"

	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/ch"
)

func DateMonitoringGETH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	date := r.URL.Query().Get("d")
	if date == "" {
		resp.BadRequest()
		return
	}

	results, err := ch.Repo.GetMonitoringInfoByDate(resp.Ctx, date)
	if err != nil {
		resp.ISE(err)
		return
	}

	resp.OK(results)
}
