package api

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/vl4deee11/ipx/lib/dns"
	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/ch"
)

type domainUploadReq struct {
	Domains []string `json:"domains" required:"true"`
}

func UploadDomainPOSTH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		resp.ISE(err)
		return
	}

	req := new(domainUploadReq)
	if err = json.Unmarshal(data, req); err != nil {
		resp.BadRequest()
		return
	}

	savedDomains := 0
	tlsConf := &tls.Config{
		//nolint:gosec // special logic flag
		InsecureSkipVerify: true,
	}
	for i := range req.Domains {
		splt := strings.Split(strings.ReplaceAll(req.Domains[i], " ", ""), ":")
		addr := ""
		switch len(splt) {
		case 1:
			addr = fmt.Sprintf("%s:%d", splt[0], 443)
		//nolint:gomnd // host with port
		case 2:
			addr = req.Domains[i]
		default:
			resp.BadRequest()
			return
		}

		conn, err := tls.Dial("tcp", addr, tlsConf)
		if err != nil {
			resp.BadRequest()
			return
		}

		certs := conn.ConnectionState().PeerCertificates
		domainsToIps := map[string][]string{}
		for i := range certs {
			domainsToIps = dns.Res.Resolve(resp.Ctx, certs[i].DNSNames)
		}

		if err := ch.Repo.Save(resp.Ctx, domainsToIps); err != nil {
			resp.ISE(err)
			return
		}

		savedDomains += len(domainsToIps)
		_ = conn.Close()
	}

	resp.OK(fmt.Sprintf("upload %d new domains", savedDomains))
}
