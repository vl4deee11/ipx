package api

import (
	"context"
	"database/sql"
	"errors"
	"net/http"

	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/pg"
)

func IPv6GraphGetBestSubnetGETH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	oct := getOctet(r)

	results, err := pg.Repo.GetBestSubnet(resp.Ctx, oct)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			resp.NotFound()
			return
		}
		resp.ISE(err)
		return
	}

	resp.OK(results)
}
