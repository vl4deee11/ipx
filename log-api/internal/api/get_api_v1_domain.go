package api

import (
	"context"
	"net/http"

	"gitlab.com/vl4deee11/ipx/log-api/internal/ch"
)

func DomainListGETH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	getListController(ctx, w, r, ch.Repo.GetDomainList)
}
