package api

import (
	"context"
	"net/http"

	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/ch"
)

func MonitoringGETH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	results, err := ch.Repo.GetMonitoringInfo(resp.Ctx)
	if err != nil {
		resp.ISE(err)
		return
	}

	resp.OK(results)
}
