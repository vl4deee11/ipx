package api

import (
	"context"
	"net/http"
	"strconv"

	"gitlab.com/vl4deee11/ipx/lib/xhttp"
)

const maxLimit = 100
const defaultOctet = 4
const defaultOffset = 0

func getOffset(r *http.Request) int {
	o := r.URL.Query().Get("o")
	offset := defaultOffset
	if o == "" {
		return offset
	}
	oi, err := strconv.Atoi(o)
	if err == nil {
		return oi
	}

	return offset
}

func getLimit(r *http.Request) int {
	l := r.URL.Query().Get("l")
	limit := maxLimit
	if l == "" {
		return limit
	}

	li, err := strconv.Atoi(l)
	if err == nil && li <= limit {
		return li
	}

	return limit
}

func getOctet(r *http.Request) int {
	o := r.URL.Query().Get("oct")
	octet := defaultOctet
	if o == "" {
		return octet
	}

	oct, err := strconv.Atoi(o)
	if err != nil {
		return octet
	}

	return oct
}

func getListController(ctx context.Context, w http.ResponseWriter, r *http.Request, cb func(context.Context, int, int) ([]string, error)) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	limit := getLimit(r)
	offset := getOffset(r)

	results, err := cb(resp.Ctx, limit, offset)
	if err != nil {
		resp.ISE(err)
		return
	}

	resp.OK(results)
}
