package api

import (
	"context"
	"net/http"

	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/ch"
)

func TopDomainListGETH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	limit := getLimit(r)
	offset := getOffset(r)

	results, err := ch.Repo.GetTopDomainList(resp.Ctx, limit, offset)
	if err != nil {
		resp.ISE(err)
		return
	}

	resp.OK(results)
}
