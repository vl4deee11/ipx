package api

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/pg"
)

type ipsv6UploadReq struct {
	IPs []string `json:"ips" required:"true"`
}

func IPv6GraphUploadPOSTH(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	resp := &xhttp.HTTPResponse{Ctx: ctx}
	resp.RW = w.(*xhttp.WriterWithStatusCode)
	if !resp.CheckAccess(r) {
		return
	}

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		resp.ISE(err)
		return
	}

	req := new(ipsv6UploadReq)
	if err = json.Unmarshal(data, req); err != nil {
		resp.BadRequest()
		return
	}

	err = pg.Repo.InsertNewIPV6s(ctx, req.IPs)
	if err != nil {
		resp.ISE(err)
		return
	}
	resp.OK(nil)
}
