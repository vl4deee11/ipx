package pg

import (
	"context"
	"strings"
)

type IPV6GraphNode struct {
	ID       int64 `db:"id"`
	ParentID int64 `db:"parent_id"`
	Weight   int64 `db:"weight"`
}

type BestSubnet struct {
	Subnet string `db:"subnet" json:"subnet"`
	Hits   int64  `db:"hits" json:"hits"`
}

func (r *repository) GetBestSubnet(ctx context.Context, oct int) (*BestSubnet, error) {
	var inf = new(BestSubnet)
	err := r.db.GetContext(ctx, inf, selectBestSubnetWithOctet, oct)
	if err != nil {
		return nil, err
	}

	return inf, err
}

func (r *repository) InsertNewIPV6s(ctx context.Context, ips []string) error {
	values := make([]string, len(ips))
	for i := range ips {
		values[i] = "('" + ips[i] + "')"
	}

	_, err := r.db.ExecContext(ctx, insertIPsV6+" "+strings.Join(values, ",")+" ON CONFLICT DO NOTHING")
	return err
}
