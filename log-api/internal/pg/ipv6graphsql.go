package pg

const selectBestSubnetWithOctet = `
SELECT net_prefix AS subnet, weight AS hits
FROM ipsv6_graph
WHERE array_length(string_to_array(net_prefix, ':'), 1) = $1
ORDER BY weight DESC
LIMIT 1;
`

const insertIPsV6 = `
INSERT INTO ipsv6 (ipv6)
VALUES 
`
