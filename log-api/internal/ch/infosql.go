package ch

const insertIPs = "INSERT INTO info (domain, ips, timestamp) VALUES (?, ?, ?);"

const selectSearchIPs = `
	SELECT ips, domain, timestamp FROM s11.info
	WHERE
`

const selectIPv6List = `
	SELECT distinct ip FROM s11.info
   		array join ips as ip
	WHERE isIPv6String(ip)
 	ORDER BY ip
`

const selectDomainList = `
	SELECT distinct domain FROM s11.info
 	ORDER BY domain
`

const selectTopDomainList = `
	SELECT splitByChar('.', domain)[-1] as domain, count() as count
	FROM s11.info
	GROUP BY domain
	ORDER BY count desc
`

const selectIPv4List = `
	SELECT distinct ip FROM s11.info
   		array join ips as ip
	WHERE isIPv4String(ip)
 	ORDER BY ip
`

const selectMonitoringByDate = `
	SELECT (
           SELECT count(*)
           FROM (
                 SELECT domain
                 FROM s11.info
                 WHERE toDate(timestamp) = toDate(?)
                 GROUP BY domain
                    )
       ) as c_domains,
       (
           SELECT count(*)
           FROM (
                 SELECT ip
                 FROM s11.info
                          array join ips as ip
                 WHERE isIPv6String(ip)
                   and toDate(timestamp) = toDate(?)
                 GROUP BY ip
                    )
       ) as c_ipsv6,
       (
           SELECT count(*)
           FROM (
                 SELECT ip
                 FROM s11.info
                          array join ips as ip
                 WHERE isIPv4String(ip)
                   and toDate(timestamp) = toDate(?)
                 GROUP BY ip
                    )
       ) as c_ipsv4
`

const selectMonitoring = `
	SELECT (
           SELECT count(*)
           FROM (
                 SELECT domain
                 FROM s11.info
                 GROUP BY domain
                    )
       ) as c_domains,
       (
           SELECT count(*)
           FROM (
                 SELECT ip
                 FROM s11.info
                          array join ips as ip
                 WHERE isIPv6String(ip)
                 GROUP BY ip
                    )
       ) as c_ipsv6,
       (
           SELECT count(*)
           FROM (
                 SELECT ip
                 FROM s11.info
                          array join ips as ip
                 WHERE isIPv4String(ip)
                 GROUP BY ip
                    )
       ) as c_ipsv4
`
