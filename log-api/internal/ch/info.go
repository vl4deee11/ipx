package ch

import (
	"context"
	"database/sql"
	"time"

	"github.com/ClickHouse/clickhouse-go"
	"gitlab.com/vl4deee11/ipx/lib/ch"
	"gitlab.com/vl4deee11/ipx/lib/log"
)

func (r *repository) Save(ctx context.Context, domainsToIPs map[string][]string) error {
	log.Logger.Info(ctx, "ch: starting save new map of resolved domains")
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.PrepareContext(ctx, insertIPs)
	if err != nil {
		return err
	}

	defer stmt.Close()

	savedIPs := 0
	for d, ips := range domainsToIPs {
		savedIPs += len(ips)
		if err := r.insertIps(ctx, stmt, ips, d); err != nil {
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	log.Logger.Infof(ctx, "ch: %d resolved domains from buffer is saved\n", len(domainsToIPs))
	return nil
}

func (r *repository) insertIps(ctx context.Context, stmt *sql.Stmt, ips []string, domain string) error {
	if _, err := stmt.ExecContext(ctx, domain, clickhouse.Array(ips), time.Now()); err != nil {
		return err
	}
	return nil
}

func (r *repository) Search(ctx context.Context, query string, limit, offset int) ([]*RawInfo, error) {
	results := make([]*RawInfo, 0, limit)

	rows, err := r.db.QueryContext(
		ctx,
		ch.BuildLimitOffset(selectSearchIPs+query, limit, offset),
	)
	if err != nil {
		return results, err
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	var (
		ips    []string
		domain string
		ts     string
	)
	for rows.Next() {
		if err := rows.Scan(&ips, &domain, &ts); err != nil {
			return results, err
		}
		results = append(results, &RawInfo{Domain: domain, Ips: ips, Timestamp: ts})
	}
	return results, nil
}

func (r *repository) GetMonitoringInfoByDate(ctx context.Context, date string) (*MonitoringInfo, error) {
	m := new(MonitoringInfo)
	rows, err := r.db.QueryContext(ctx, selectMonitoringByDate, date, date, date)
	if err != nil {
		return nil, err
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	var (
		cDomains uint64
		cIpsv6   uint64
		cIpsv4   uint64
	)
	for rows.Next() {
		if err = rows.Scan(&cDomains, &cIpsv6, &cIpsv4); err != nil {
			return nil, err
		}
	}
	m.CDomains = cDomains
	m.CIpsv6 = cIpsv6
	m.CIpsv4 = cIpsv4
	return m, nil
}

func (r *repository) GetMonitoringInfo(ctx context.Context) (*MonitoringInfo, error) {
	m := new(MonitoringInfo)
	rows, err := r.db.QueryContext(ctx, selectMonitoring)
	if err != nil {
		return nil, err
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	var (
		cDomains uint64
		cIpsv6   uint64
		cIpsv4   uint64
	)
	for rows.Next() {
		if err := rows.Scan(&cDomains, &cIpsv6, &cIpsv4); err != nil {
			return nil, err
		}
	}
	m.CDomains = cDomains
	m.CIpsv6 = cIpsv6
	m.CIpsv4 = cIpsv4
	return m, nil
}

func (r *repository) GetIpv4List(ctx context.Context, limit, offset int) ([]string, error) {
	return r.getList(ctx, selectIPv4List, limit, offset)
}

func (r *repository) GetIpv6List(ctx context.Context, limit, offset int) ([]string, error) {
	return r.getList(ctx, selectIPv6List, limit, offset)
}

func (r *repository) GetDomainList(ctx context.Context, limit, offset int) ([]string, error) {
	return r.getList(ctx, selectDomainList, limit, offset)
}

func (r *repository) GetTopDomainList(ctx context.Context, limit, offset int) ([]*RawDomainStats, error) {
	res := make([]*RawDomainStats, 0, limit)

	rows, err := r.db.QueryContext(ctx, ch.BuildLimitOffset(selectTopDomainList, limit, offset))
	if err != nil {
		return res, err
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	var (
		domain string
		c      uint64
	)
	for rows.Next() {
		if err = rows.Scan(&domain, &c); err != nil {
			return res, err
		}
		res = append(res, &RawDomainStats{
			Domain: domain,
			Count:  c,
		})
	}
	return res, nil
}

func (r *repository) getList(ctx context.Context, query string, limit, offset int) ([]string, error) {
	res := make([]string, 0, limit)
	rows, err := r.db.QueryContext(ctx, ch.BuildLimitOffset(query, limit, offset))
	if err != nil {
		return nil, err
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	var s string
	for rows.Next() {
		if err = rows.Scan(&s); err != nil {
			return res, err
		}
		res = append(res, s)
	}
	return res, nil
}
