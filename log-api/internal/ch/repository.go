package ch

import (
	"database/sql"
	"errors"

	"gitlab.com/vl4deee11/ipx/lib/ch"
)

type repository struct {
	db *sql.DB
}

var Repo *repository

func NewRepository(host, dbName, user, pass string, port uint16) error {
	if host == "" {
		return errors.New("ch: host == <empty string>")
	}

	if dbName == "" {
		return errors.New("ch: dbName == <empty string>")
	}
	Repo = &repository{}
	db, err := ch.Conn(host, dbName, user, pass, port)
	if err != nil {
		return err
	}
	Repo.db = db

	return nil
}
