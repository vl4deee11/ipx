package ch

type RawInfo struct {
	Domain    string   `json:"domain"`
	Ips       []string `json:"ips"`
	Timestamp string   `json:"timestamp"`
}

type RawDomainStats struct {
	Domain string `json:"domain"`
	Count  uint64 `json:"c"`
}

type MonitoringInfo struct {
	CIpsv6   uint64 `json:"c_ipsv6"`
	CIpsv4   uint64 `json:"c_ipsv4"`
	CDomains uint64 `json:"c_domains"`
}
