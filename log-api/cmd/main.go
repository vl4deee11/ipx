package main

import (
	"context"
	"fmt"
	"net/http"
	"regexp"

	"gitlab.com/vl4deee11/ipx/lib/cache"
	"gitlab.com/vl4deee11/ipx/lib/ch"
	"gitlab.com/vl4deee11/ipx/lib/dns"
	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/ipxlang"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/lib/pg"
	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-api/internal/api"
	sch "gitlab.com/vl4deee11/ipx/log-api/internal/ch"
	spg "gitlab.com/vl4deee11/ipx/log-api/internal/pg"

	"github.com/kelseyhightower/envconfig"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Config struct {
	Log   log.Conf
	Cache cache.Conf
	CH    ch.Conf
	PG    pg.Conf
	DNS   dns.Conf

	HTTP struct {
		Token string `required:"true"`
		Port  uint16 `required:"true"`
	}
}

func main() {
	c, err := initialize()
	if err != nil {
		panic(err)
	}

	log.MakeLogger(c.Log.LVL)
	fmt.Println("main: service is start v=2.0.0")

	if err = spg.NewRepository(c.PG.Host, c.PG.User, c.PG.Pass, c.PG.DBName, c.PG.Port); err != nil {
		log.Logger.Fatal(err)
		return
	}

	if err = sch.NewRepository(c.CH.Host, c.CH.DBName, c.CH.User, c.CH.Pass, c.CH.Port); err != nil {
		log.Logger.Fatal(err)
		return
	}

	if err := dns.NewResolver(c.DNS.Addr); err != nil {
		log.Logger.Fatal(err)
		return
	}

	if err := ipxlang.NewCompiler(); err != nil {
		log.Logger.Fatal(err)
		return
	}

	if err := cache.NewCacheCli(
		c.Cache.Host, c.Cache.Port, c.Cache.DBN, c.Cache.TTL,
	); err != nil {
		log.Logger.Fatal(err)
		return
	}

	if err := cache.Cli.SetWithoutTTL(context.Background(), c.HTTP.Token, c.HTTP.Token); err != nil {
		log.Logger.Fatal(err)
		return
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", route)
	mux.Handle("/metrics", promhttp.Handler())

	// Add the pprof routes
	// mux.HandleFunc("/debug/pprof/", pprof.Index)
	// mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	// mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	// mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	// mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	//
	// mux.Handle("/debug/pprof/block", pprof.Handler("block"))
	// mux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	// mux.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	// mux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	log.Logger.Info(context.Background(), "main: service is up")
	log.Logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", c.HTTP.Port), mux))
}

func initialize() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("log_api", c)
	if err != nil {
		return nil, err
	}
	return c, nil
}

var rSearch = regexp.MustCompile(`/api/v1/search.*`)

var rDateMonitoring = regexp.MustCompile(`/api/v1/date-monitoring.*`)
var rBestSubnet = regexp.MustCompile(`/api/v1/graph-ipv6/best-subnet.*`)
var rUploadIPv6 = regexp.MustCompile(`/api/v1/graph-ipv6/upload.*`)
var rMonitoring = regexp.MustCompile(`/api/v1/monitoring.*`)

var rIPv4 = regexp.MustCompile(`/api/v1/ipv4.*`)
var rIPv6 = regexp.MustCompile(`/api/v1/ipv6.*`)
var rDomain = regexp.MustCompile(`/api/v1/domain.*`)
var rDomainTop = regexp.MustCompile(`/api/v1/top.*`)

func route(w http.ResponseWriter, r *http.Request) {
	lw := xhttp.NewWriter(w, r.URL.Path)
	ctx := fpctx.APICtx()
	switch {
	case r.URL.Path == "/health" || r.URL.Path == "/ready":
		lw.WriteHeader(http.StatusOK)
		_, _ = fmt.Fprintf(lw, "ok")

	case rSearch.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.SearchGETH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	case rDateMonitoring.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.DateMonitoringGETH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	case rMonitoring.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.MonitoringGETH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	case rIPv4.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.IPv4ListGETH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	case rIPv6.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.IPv6ListGETH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	case rDomain.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.DomainListGETH(ctx, lw, r)
		} else if r.Method == http.MethodPost {
			api.UploadDomainPOSTH(ctx, lw, r)
		}
	case rDomainTop.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.TopDomainListGETH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	case rBestSubnet.MatchString(r.URL.Path):
		if r.Method == http.MethodGet {
			api.IPv6GraphGetBestSubnetGETH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	case rUploadIPv6.MatchString(r.URL.Path):
		if r.Method == http.MethodPost {
			api.IPv6GraphUploadPOSTH(ctx, lw, r)
		} else {
			lw.WriteHeader(http.StatusMethodNotAllowed)
		}
	default:
		lw.WriteHeader(http.StatusNotFound)
		_, _ = fmt.Fprintf(lw, "404 not found")
	}
	log.Logger.Infof(
		ctx,
		"api: %s %s%s [%d]",
		r.Method,
		r.Host,
		r.URL.String(),
		lw.StatusCode,
	)
}
