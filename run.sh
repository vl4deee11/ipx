# run log-inspector
cd ./log-inspector/
touch logs.txt
echo "" >logs.txt
./r.sh >>logs.txt &
echo "=======Run ipx/log-inspector OK======="
sleep 10
cat logs.txt
echo "======================================"
cd ../

# run log-x-inspector
cd ./log-x-inspector/
touch logs.txt
echo "" >logs.txt
./r.sh >>logs.txt &
echo "=======Run ipx/log-x-inspector OK======="
sleep 10
cat logs.txt
echo "========================================="
cd ../

# run ipv6-scan
cd ./log-ipv6-scan/
touch logs.txt
echo "" >logs.txt
./r.sh >>logs.txt &
echo "=======Run ipx/log-ipv6-scan OK======="
sleep 10
cat logs.txt
echo "==================================="
cd ../

# run log-api
cd ./log-api/
touch logs.txt
echo "" >logs.txt
./r.sh >>logs.txt &
echo "=======Run ipx/log-api OK======="
sleep 10
cat logs.txt
echo "==================================="
cd ../

# run log-dwh
cd ./log-dwh/
touch logs.txt
echo "" >logs.txt
./r.sh >>logs.txt &
echo "=======Run ipx/log-dwh OK======="
sleep 10
cat logs.txt
echo "==================================="

cd ../

echo "=======Check PORTS======="
lsof -i -P -n | grep LISTEN

#docker run --name clickhouse-server --restart unless-stopped -p 8123:8123 -p 9000:9000 -e CLICKHOUSE_DB=s11 -e CLICKHOUSE_USER=s11 -e CLICKHOUSE_PASSWORD=s11 --ulimit nofile=262144:262144 -d  altinity/clickhouse-server:21.12.3.32.altinitydev.arm
## or
#docker run --name clickhouse-server --restart unless-stopped -p 8123:8123 -p 9000:9000 -e CLICKHOUSE_DB=s11 -e CLICKHOUSE_USER=s11 -e CLICKHOUSE_PASSWORD=s11 --ulimit nofile=262144:262144 -d clickhouse/clickhouse-server:21.3.20-alpine
#
#docker run --name postgres-server --restart unless-stopped -p 5432:5432 -e POSTGRES_PASSWORD=s11 -e  POSTGRES_USER=s11 -e POSTGRES_DB=s11 -d postgres:11.12-alpine3.14
#docker run --name redis-server  --restart unless-stopped -p 6379:6379 -d redis:6.2-alpine
