package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	eventbus "gitlab.com/vl4deee11/ipx/lib/event-bus"

	"gitlab.com/vl4deee11/ipx/lib/dns"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-inspector/internal/stream"
	"gitlab.com/vl4deee11/ipx/log-inspector/internal/websocket"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Log    log.Conf
	EB     eventbus.Conf
	DNS    dns.Conf
	Stream struct {
		Concurrency int `required:"true"`
		BuffSize    int `split_words:"true" required:"true"`
	}

	WS struct {
		HandshakeTimeout time.Duration `split_words:"true" required:"true"`
		RetryPeriod      time.Duration `split_words:"true" required:"true"`
		ReadPeriod       time.Duration `split_words:"true" required:"true"`
		URL              string
	}
	HTTP struct {
		Port uint16 `required:"true"`
	}
}

func main() {
	c, err := initialize()
	if err != nil {
		panic(err)
	}

	log.MakeLogger(c.Log.LVL)
	fmt.Println("main: service is start v=3.0.0")

	if err = dns.NewResolver(c.DNS.Addr); err != nil {
		log.Logger.Fatal(err)
		return
	}

	wsC, err := websocket.NewConsumer(
		c.WS.URL, c.WS.ReadPeriod, c.WS.RetryPeriod, c.WS.HandshakeTimeout,
	)
	if err != nil {
		log.Logger.Fatal(err)
		return
	}

	outC, errC := wsC.Consume()

	handler, err := stream.NewHandler(
		outC, errC, c.Stream.BuffSize, c.Stream.Concurrency, c.EB.Brokers,
	)
	if err != nil {
		log.Logger.Fatal(err)
		return
	}

	handler.Handle()
	mux := http.NewServeMux()
	mux.HandleFunc("/", xhttp.MSRoute)
	mux.Handle("/metrics", promhttp.Handler())

	// Add the pprof routes
	// mux.HandleFunc("/debug/pprof/", pprof.Index)
	// mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	// mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	// mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	// mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	//
	// mux.Handle("/debug/pprof/block", pprof.Handler("block"))
	// mux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	// mux.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	// mux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	log.Logger.Info(context.Background(), "main: service is up")
	log.Logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", c.HTTP.Port), mux))
}

func initialize() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("log_inspector", c)
	if err != nil {
		return nil, err
	}
	return c, nil
}
