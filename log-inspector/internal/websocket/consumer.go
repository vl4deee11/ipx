package websocket

import (
	"errors"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/common"

	"github.com/gorilla/websocket"
)

type Consumer struct {
	URL string
	// Time settings
	readPeriod       time.Duration
	retryPeriod      time.Duration
	handshakeTimeout time.Duration
}

func NewConsumer(_url string, readPeriod, retryPeriod, handshakeTimeout time.Duration) (*Consumer, error) {
	if _url == "" {
		return nil, errors.New("ws: url == <empty string>")
	}

	return &Consumer{
		URL:              _url,
		readPeriod:       readPeriod,
		retryPeriod:      retryPeriod,
		handshakeTimeout: handshakeTimeout,
	}, nil
}

func (c *Consumer) Consume() (outC chan *common.Cert, errC chan error) {
	outC = make(chan *common.Cert)
	errC = make(chan error)

	go func() {
		for {
			dialer := &websocket.Dialer{HandshakeTimeout: c.handshakeTimeout}
			conn, _, err := dialer.Dial(c.URL, nil)

			if err != nil {
				errC <- err
				time.Sleep(c.retryPeriod)
				continue
			}

			stopC := make(chan struct{})

			r := &reader{
				conn:       conn,
				outC:       outC,
				errC:       errC,
				readPeriod: c.readPeriod,
			}
			r.Read()

			close(stopC)
			conn.Close()
		}
	}()
	return
}
