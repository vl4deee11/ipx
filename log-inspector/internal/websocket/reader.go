package websocket

import (
	"time"

	"gitlab.com/vl4deee11/ipx/lib/common"
	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/log"

	"github.com/gorilla/websocket"
)

type reader struct {
	conn       *websocket.Conn
	outC       chan *common.Cert
	errC       chan error
	readPeriod time.Duration
}

type resp struct {
	MsgType string      `json:"message_type"`
	Cert    common.Cert `json:"data"`
}

func (r *reader) Read() {
	ctx := fpctx.WorkerCtxEndless()
	log.Logger.Debug(ctx, "wsreader: start new ws reader")
	res := resp{}
	for {
		_ = r.conn.SetReadDeadline(time.Now().Add(r.readPeriod))
		err := r.conn.ReadJSON(&res)

		if err != nil {
			r.errC <- err
			break
		}

		if res.MsgType == "heartbeat" {
			continue
		}

		if len(res.Cert.LeafCert.AllDomains) == 0 {
			continue
		}

		r.outC <- &res.Cert
	}
	log.Logger.Debug(ctx, "wsreader: stop")
}
