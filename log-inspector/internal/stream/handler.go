package stream

import (
	"context"
	"errors"
	"time"

	eventbus "gitlab.com/vl4deee11/ipx/lib/event-bus"
	"gitlab.com/vl4deee11/ipx/lib/event-bus/models"

	"gitlab.com/vl4deee11/ipx/lib/common"
	"gitlab.com/vl4deee11/ipx/lib/dns"
	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/log"
)

type Handler struct {
	outC chan *common.Cert
	errC chan error

	buffSize    int
	concurrency int

	producer eventbus.ProducerI
}

func NewHandler(
	outC chan *common.Cert,
	errC chan error,
	buffSize, concurrency int,
	brokers []string,
) (*Handler, error) {
	if outC == nil {
		return nil, errors.New("streamhandler: out chan == nil")
	}

	if errC == nil {
		return nil, errors.New("streamhandler: out chan == nil")
	}

	if buffSize < 1 {
		return nil, errors.New("streamhandler: bufferSize < 1")
	}
	if concurrency < 1 {
		return nil, errors.New("streamhandler: concurrency < 1")
	}

	if len(brokers) == 0 {
		return nil, errors.New("streamhandler: len(brokers) == 0")
	}

	return &Handler{
		outC:        outC,
		errC:        errC,
		concurrency: concurrency,
		buffSize:    buffSize,
		producer:    eventbus.NewProducer(brokers, models.TopicIpxDomainDNSV1, buffSize),
	}, nil
}

func (h *Handler) Handle() {
	for i := 0; i < h.concurrency; i++ {
		go h.outReader()
	}
	go h.errReader()
}

func (h *Handler) outReader() {
	var (
		buff = make([]*common.Cert, h.buffSize)
		bi   = 0
		ctx  context.Context
	)

	for r := range h.outC {
		buff[bi] = &(*r)
		bi++
		if bi >= h.buffSize {
			ctx = fpctx.WorkerCtxEndless()
			h.produceRetry(ctx, dns.Res.ResolveFromCert(ctx, buff))
			bi = 0
		}
	}
}

func (h *Handler) produceRetry(ctx context.Context, domainsToIPs map[string][]string) {
	msgs := make([]*models.IpxDomainDNSV1Event, 0, len(domainsToIPs))
	now := time.Now()
	for dom := range domainsToIPs {
		msgs = append(msgs, &models.IpxDomainDNSV1Event{
			Domain: dom,
			IPs:    domainsToIPs[dom],
			Time:   now,
		})
	}

	i := 0
	for i < len(msgs) {
		if err := h.producer.Produce(ctx, msgs[i]); err != nil {
			log.Logger.Errorf(ctx, "streamhandler: produce domains. ERR => %s\n", err.Error())
		} else {
			i++
		}
	}
}

func (h *Handler) errReader() {
	for e := range h.errC {
		log.Logger.Errorf(fpctx.WorkerCtxEndless(), "streamhandler: error from ws. ERR => %s\n", e)
	}
}
