import ipaddress
import networkx as nx

mclr = {(0, 1): 'red', (1, 2): 'blue', (2, 3): 'green', (3, 4): 'yellow', (4, 5): 'purple', (5, 6): 'black',
        (6, 7): 'orange'}


def main():
    g = nx.Graph()
    with open("ips.csv") as file:
        x=0
        for line in file:
            ip = ipaddress.ip_address(line.rstrip())
            if ip.is_link_local or ip.is_private or ip.is_multicast: continue
            octets = ip.exploded.split(":")
            print(len(ip.exploded))
            for i in range(len(octets)):
                cur = f'{octets[i]}'
                if i != 0:
                    cur += f':{octets[i]}'
                g.add_node(cur)

            cur = f'{octets[i]}'
            for i in range(1, len(octets)):
                pr = cur
                cur += f':{octets[i]}'
                d = g.get_edge_data(pr, cur)
                w = 1
                if d is not None:
                    w = d["weight"] + 1
                g.add_edge(pr, cur, weight=w, label=w, color=mclr[(i - 1, i)])

    for e in g.edges:
        d = g.get_edge_data(e[0], e[1])
        if d["weight"] <= 50:
            g.remove_edges_from([e])
    le = sorted(list(dict(g.edges).items()), key=lambda x:x[1]["weight"])[::-1]
    for e in le[:50]:
        print("E = ", e[0], "W = ", e[1]["weight"])
    g.remove_nodes_from(list(nx.isolates(g)))
    nx.nx_agraph.write_dot(g, "./graph.dot")


main()
