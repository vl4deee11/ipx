export LOG_IPV6_SCAN_GRAPH_PROC_CONCURRENCY=300
export LOG_IPV6_SCAN_GRAPH_PROC_WAIT_TIME=5s

export LOG_IPV6_SCAN_HTTP_SCAN_PROC_CONCURRENCY=1
export LOG_IPV6_SCAN_HTTP_SCAN_PROC_WAIT_TIME=5s

export LOG_IPV6_SCAN_DOMAIN_DNS_CONSUMER_WAIT_TIME=5s

export LOG_IPV6_SCAN_TASK_GEN_PROC_CONCURRENCY=1
export LOG_IPV6_SCAN_TASK_GEN_PROC_OCTET=7
export LOG_IPV6_SCAN_TASK_GEN_PROC_COOLING_TIME=24h
export LOG_IPV6_SCAN_TASK_GEN_PROC_WAIT_TIME=5s

export LOG_IPV6_SCAN_LOG_LVL=debug

export LOG_IPV6_SCAN_EB_BROKERS=0.0.0.0:9092

export LOG_IPV6_SCAN_PG_DB_NAME=s11
export LOG_IPV6_SCAN_PG_PORT=5432
export LOG_IPV6_SCAN_PG_USER=s11
export LOG_IPV6_SCAN_PG_PASS=s11
export LOG_IPV6_SCAN_PG_HOST=0.0.0.0

export LOG_IPV6_SCAN_HTTP_PORT=8083
export GODEBUG=madvdontneed=1

go run ./cmd/main.go
