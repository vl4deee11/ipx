package httpscanproc

import (
	"context"
	"database/sql"
	"errors"
	"net"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/pg"
)

type HTTPScanProc struct {
	concurrency int
	waitTime    time.Duration
}

func NewProc(concurrency int, waitTime time.Duration) *HTTPScanProc {
	return &HTTPScanProc{concurrency: concurrency, waitTime: waitTime}
}

func (p *HTTPScanProc) Process() {
	for i := 0; i < p.concurrency; i++ {
		go p.proc()
	}
}

func (p *HTTPScanProc) proc() {
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)
	for {
		ctx, cancel = fpctx.WorkerCtx()
		ipv6, err := pg.Repo.GetUnScannedIPv6(ctx)
		if err != nil {
			cancel()
			if errors.Is(err, sql.ErrNoRows) {
				time.Sleep(p.waitTime)
				continue
			}
			time.Sleep(p.waitTime)
			log.Logger.Errorf(ctx, "httpscanproc: get new net prefix tx. ERR => %s\n", err.Error())
			continue
		}

		conn, err := net.DialTimeout("tcp6", "["+ipv6+"]:80", 10*time.Second)
		if err != nil {
			log.Logger.Debugf(ctx, "httpscanproc: dial. ERR => %s\n", err.Error())

			err = pg.Repo.SetIPv6IsAlive(ctx, ipv6, false)
			if err != nil {
				log.Logger.Errorf(ctx, "httpscanproc: delete ipv6. ERR => %s\n", err.Error())
				time.Sleep(p.waitTime)
			}
			cancel()
			continue
		}

		conn.Close()
		log.Logger.Infof(ctx, "httpscanproc: successfully scan ipv6 = %s", ipv6)

		err = pg.Repo.SetIPv6IsAlive(ctx, ipv6, true)
		if err != nil {
			log.Logger.Errorf(ctx, "httpscanproc: set is_alive. ERR => %s\n", err.Error())
			time.Sleep(p.waitTime)
		}

		cancel()
	}
}
