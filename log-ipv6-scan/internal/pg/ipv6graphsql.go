package pg

const selectForUpdateNewIPV6 = `
SELECT ipv6
FROM ipsv6
WHERE handled_at IS NULL
  AND is_alive = TRUE FOR UPDATE OF ipsv6 SKIP LOCKED
LIMIT 1;
`

const selectForUpdateIPV6Prefix = `
SELECT id, weight
FROM ipsv6_graph
WHERE net_prefix = $1 FOR UPDATE OF ipsv6_graph
LIMIT 1;`

const insertIPV6Prefix = `
INSERT INTO ipsv6_graph(parent_id, net_prefix, weight)
VALUES ($1, $2, $3)  RETURNING id, parent_id, weight;`

const updateIPV6Prefix = `
UPDATE ipsv6_graph
SET weight = $1
WHERE id = $2;`

const updateNewIPV6SetHandled = `
UPDATE ipsv6
SET handled_at = NOW()
WHERE ipv6 = $1;
`

const insertIPsV6 = `
INSERT INTO ipsv6 (ipv6)
VALUES 
`

const selectBestSubnetWithOctetAndCoolingTime = `
SELECT net_prefix, id, last_processed_at
FROM ipsv6_graph
WHERE array_length(string_to_array(net_prefix, ':'), 1) = $1
ORDER BY weight DESC FOR UPDATE OF ipsv6_graph SKIP LOCKED
LIMIT 1;
`

const updateBestSubnetLastProcessedAt = `
UPDATE ipsv6_graph
SET last_processed_at = NOW()
WHERE id = $1;
`
const insertNewUnScannedIPsV6 = `
INSERT INTO ipsv6 (ipv6, is_alive)
VALUES ($1, NULL) ON CONFLICT DO NOTHING;
`
const selectUnScannedIPv6 = `
SELECT ipv6
FROM ipsv6
WHERE is_alive IS NULL
  AND (locked_at IS NULL OR
       locked_at < NOW() - 500 * INTERVAL '1 SECOND')
    FOR UPDATE OF ipsv6 SKIP LOCKED
LIMIT 1;`

const updateSetIPv6Locked = `
UPDATE ipsv6
SET locked_at = NOW()
WHERE ipv6 = $1;`

const updateSetIPv6ISAlive = `
UPDATE ipsv6
SET is_alive = $2
WHERE ipv6 = $1;`
