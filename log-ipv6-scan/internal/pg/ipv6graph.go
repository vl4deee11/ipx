package pg

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net"
	"strings"
	"time"
	"unsafe"

	"github.com/jmoiron/sqlx"
)

type IPV6GraphNode struct {
	ID       int64 `db:"id"`
	ParentID int64 `db:"parent_id"`
	Weight   int64 `db:"weight"`
}

func (r *repository) InsertNewIPV6TX(ctx context.Context) error {
	var (
		ipv6 string
		err  error
		tx   *sqlx.Tx
	)

	tx, err = r.db.BeginTxx(ctx, nil)
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				return
			}
			err = tx.Rollback()
		}
	}()

	err = tx.GetContext(ctx, &ipv6, selectForUpdateNewIPV6)
	if err != nil {
		return err
	}

	ipv6x := net.ParseIP(ipv6)
	exp := strings.Split(fmt.Sprintf("%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x",
		ipv6x[0],
		ipv6x[1],
		ipv6x[2],
		ipv6x[3],
		ipv6x[4],
		ipv6x[5],
		ipv6x[6],
		ipv6x[7],
		ipv6x[8],
		ipv6x[9],
		ipv6x[10],
		ipv6x[11],
		ipv6x[12],
		ipv6x[13],
		ipv6x[14],
		ipv6x[15],
	), ":")

	pb := make([]byte, 0, (len(exp)*4)+7)
	var prev IPV6GraphNode
	for i := range exp {
		if i != 0 {
			pb = append(pb, 58)
		}
		pb = append(pb, exp[i]...)

		var node IPV6GraphNode
		err = tx.GetContext(ctx, &node, selectForUpdateIPV6Prefix, *(*string)(unsafe.Pointer(&pb)))
		isErrNoRows := errors.Is(err, sql.ErrNoRows)
		if err != nil && !isErrNoRows {
			return err
		}

		if isErrNoRows {
			err = tx.GetContext(ctx, &node, insertIPV6Prefix, prev.ID, *(*string)(unsafe.Pointer(&pb)), 1)
			if err != nil {
				return err
			}
		} else {
			_, err = tx.ExecContext(ctx, updateIPV6Prefix, node.Weight+1, node.ID)
			if err != nil {
				return err
			}
		}
		prev = node
	}

	_, err = tx.ExecContext(ctx, updateNewIPV6SetHandled, ipv6)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

func (r *repository) InsertNewIPV6s(ctx context.Context, ips []string) error {
	values := make([]string, len(ips))
	for i := range ips {
		values[i] = "('" + ips[i] + "')"
	}

	_, err := r.db.ExecContext(ctx, insertIPsV6+" "+strings.Join(values, ",")+" ON CONFLICT DO NOTHING")
	return err
}

type BestSubnetForGeneration struct {
	NetPrefix       string       `db:"net_prefix" `
	ID              int64        `db:"id"`
	LastProcessedAt sql.NullTime `db:"last_processed_at"`
}

func (r *repository) GetNetPrefixForTaskGen(ctx context.Context, oct int, coolingTime time.Duration) (string, error) {
	var (
		subnet BestSubnetForGeneration
		err    error
		tx     *sqlx.Tx
	)

	tx, err = r.db.BeginTxx(ctx, nil)
	if err != nil {
		return subnet.NetPrefix, err
	}

	err = tx.GetContext(ctx, &subnet, selectBestSubnetWithOctetAndCoolingTime, oct)
	if err != nil {
		return subnet.NetPrefix, err
	}

	if subnet.LastProcessedAt.Valid && subnet.LastProcessedAt.Time.Add(coolingTime).After(time.Now()) {
		return "", sql.ErrNoRows
	}

	_, err = tx.ExecContext(ctx, updateBestSubnetLastProcessedAt, subnet.ID)
	if err != nil {
		return subnet.NetPrefix, err
	}

	err = tx.Commit()
	if err != nil {
		return subnet.NetPrefix, err
	}

	return subnet.NetPrefix, nil
}

func (r *repository) InsertNewIPV6UnScanned(ctx context.Context, ipv6 string) error {
	_, err := r.db.ExecContext(ctx, insertNewUnScannedIPsV6, ipv6)
	return err
}

func (r *repository) GetUnScannedIPv6(ctx context.Context) (string, error) {
	var (
		ipv6 string
		err  error
		tx   *sqlx.Tx
	)

	tx, err = r.db.BeginTxx(ctx, nil)
	if err != nil {
		return ipv6, err
	}

	err = tx.GetContext(ctx, &ipv6, selectUnScannedIPv6)
	if err != nil {
		return ipv6, err
	}

	_, err = tx.ExecContext(ctx, updateSetIPv6Locked, ipv6)
	if err != nil {
		return ipv6, err
	}

	err = tx.Commit()

	return ipv6, err
}

func (r *repository) SetIPv6IsAlive(ctx context.Context, ipv6 string, isAlive bool) error {
	_, err := r.db.ExecContext(ctx, updateSetIPv6ISAlive, ipv6, isAlive)
	return err
}
