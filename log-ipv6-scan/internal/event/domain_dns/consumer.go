package domaindns

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"github.com/segmentio/kafka-go"
	eb "gitlab.com/vl4deee11/ipx/lib/event-bus"
	"gitlab.com/vl4deee11/ipx/lib/event-bus/models"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/pg"
)

const timeout = 60 * time.Second

type Handler struct {
	waitTime time.Duration
}

func NewConsumer(brokers []string, servName string, waitTime time.Duration) eb.ConsumerI {
	return eb.NewConsumer(brokers, servName, models.TopicIpxDomainDNSV1, kafka.FirstOffset, &Handler{waitTime: waitTime})
}

func (c *Handler) Handle(msg kafka.Message) bool {
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	model := &models.IpxDomainDNSV1Event{}

	err = json.Unmarshal(msg.Value, model)
	if err != nil {
		log.Logger.Errorf(ctx, "domain_dns_consumer: json unmarshall %s. ERR => %s\n", string(msg.Value), err.Error())
		time.Sleep(c.waitTime)
		return true
	}

	ipsv6 := make([]string, 0)
	for i := 0; i < len(model.IPs); i++ {
		if strings.Contains(model.IPs[i], ":") {
			ipsv6 = append(ipsv6, model.IPs[i])
		}
	}

	if len(ipsv6) == 0 {
		log.Logger.Info(ctx, "domain_dns_consumer: ipv6 not found in dns results")
		return true
	}

	err = pg.Repo.InsertNewIPV6s(ctx, ipsv6)
	if err != nil {
		log.Logger.Errorf(ctx, "domain_dns_consumer: ipv6 insert new ipsv6. ERR => %s\n", err.Error())
		time.Sleep(c.waitTime)
		return false
	}

	log.Logger.Infof(ctx, "domain_dns_consumer: ipv6 insert %d new ipsv6 ", len(ipsv6))
	return true
}
