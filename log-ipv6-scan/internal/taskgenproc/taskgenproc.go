package taskgenproc

import (
	"context"
	"database/sql"
	"errors"
	"net"
	"strconv"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/pg"
)

type TaskGetProc struct {
	concurrency int
	octet       int
	waitTime    time.Duration
	coolingTime time.Duration
}

func NewProc(concurrency, octet int, waitTime, coolingTime time.Duration) (*TaskGetProc, error) {
	if octet < 0 || octet > 7 {
		return nil, errors.New("taskgenproc: octet < 0 || octet > 7")
	}
	return &TaskGetProc{
		concurrency: concurrency,
		waitTime:    waitTime,
		octet:       octet,
		coolingTime: coolingTime,
	}, nil
}

func (p *TaskGetProc) Process() {
	for i := 0; i < p.concurrency; i++ {
		go p.proc()
	}
}

func (p *TaskGetProc) proc() {
	var (
		ctx context.Context
	)
	for {
		ctx = fpctx.WorkerCtxEndless()
		netPrefix, err := pg.Repo.GetNetPrefixForTaskGen(ctx, p.octet, p.coolingTime)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				time.Sleep(p.waitTime)
				continue
			}
			log.Logger.Errorf(ctx, "taskgenproc: get new net prefix tx. ERR => %s\n", err.Error())
			continue
		}
		err = p.generateRange(ctx, netPrefix)
		if err != nil {
			log.Logger.Errorf(ctx, "taskgenproc: get new net prefix tx. ERR => %s\n", err.Error())
			continue
		}
		log.Logger.Info(ctx, "taskgenproc: generate new ipv6 successfully")
	}
}

func (p *TaskGetProc) generateRange(ctx context.Context, netPrefix string) error {
	netMask := netPrefix + "::/" + strconv.Itoa(p.octet*16)
	ip, _, err := net.ParseCIDR(netMask)
	if err != nil {
		return err
	}

	log.Logger.Infof(ctx, "taskgenproc: generate for mask = %s\n", netMask)
	sti := p.octet * 2
	var backtracking func(int) error
	backtracking = func(sti int) error {
		if sti >= 16 {
			return pg.Repo.InsertNewIPV6UnScanned(ctx, ip.String())
		}
		for fh := 0; fh < 256; fh++ {
			ip[sti] = byte(fh)
			if err = backtracking(sti + 1); err != nil {
				return err
			}
		}
		return nil
	}
	return backtracking(sti)
}
