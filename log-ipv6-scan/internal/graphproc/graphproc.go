package graphproc

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/pg"
)

type GraphProc struct {
	concurrency int
	waitTime    time.Duration
}

func NewProc(concurrency int, waitTime time.Duration) *GraphProc {
	return &GraphProc{concurrency: concurrency, waitTime: waitTime}
}

func (p *GraphProc) Process() {
	for i := 0; i < p.concurrency; i++ {
		go p.proc()
	}
}

func (p *GraphProc) proc() {
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)
	for {
		ctx, cancel = fpctx.WorkerCtx()
		err := pg.Repo.InsertNewIPV6TX(ctx)
		if err != nil {
			cancel()
			if errors.Is(err, sql.ErrNoRows) {
				continue
			}
			log.Logger.Errorf(ctx, "graphproc: insert new ipv6 tx. ERR => %s\n", err.Error())
			time.Sleep(p.waitTime)
		}
		log.Logger.Info(ctx, "graphproc: insert new ipv6 successfully")
	}
}
