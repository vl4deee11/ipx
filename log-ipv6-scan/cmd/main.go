package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	domaindns "gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/event/domain_dns"

	eventbus "gitlab.com/vl4deee11/ipx/lib/event-bus"

	"gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/graphproc"
	"gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/httpscanproc"

	"gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/taskgenproc"

	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/lib/pg"
	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	spg "gitlab.com/vl4deee11/ipx/log-ipv6-scan/internal/pg"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/kelseyhightower/envconfig"
)

const (
	serviceName = "log_ipv6_scan"
)

type Config struct {
	Log log.Conf
	PG  pg.Conf
	EB  eventbus.Conf

	HTTP struct {
		Port uint16 `required:"true"`
	}

	GraphProc struct {
		Concurrency int           `required:"true"`
		WaitTime    time.Duration `required:"true" split_words:"true"`
	} `split_words:"true" required:"true"`

	HTTPScanProc struct {
		Concurrency int           `required:"true"`
		WaitTime    time.Duration `required:"true" split_words:"true"`
	} `split_words:"true" required:"true"`

	TaskGenProc struct {
		Concurrency int           `required:"true"`
		Octet       int           `required:"true"`
		WaitTime    time.Duration `required:"true" split_words:"true"`
		CoolingTime time.Duration `required:"true" split_words:"true"`
	} `split_words:"true" required:"true"`

	DomainDnsConsumer struct {
		WaitTime time.Duration `required:"true" split_words:"true"`
	} `split_words:"true" required:"true"`
}

func main() {
	c, err := initialize()
	if err != nil {
		panic(err)
	}

	log.MakeLogger(c.Log.LVL)
	fmt.Println("main: service is start v=3.0.0")

	if err = spg.NewRepository(c.PG.Host, c.PG.User, c.PG.Pass, c.PG.DBName, c.PG.Port); err != nil {
		log.Logger.Fatal(err)
		return
	}

	gp := graphproc.NewProc(c.GraphProc.Concurrency, c.GraphProc.WaitTime)
	hs := httpscanproc.NewProc(c.HTTPScanProc.Concurrency, c.HTTPScanProc.WaitTime)
	tg, err := taskgenproc.NewProc(c.TaskGenProc.Concurrency, c.TaskGenProc.Octet, c.TaskGenProc.WaitTime, c.TaskGenProc.CoolingTime)
	if err != nil {
		log.Logger.Fatal(err)
		return
	}

	domainDnsCons := domaindns.NewConsumer(c.EB.Brokers, serviceName, c.DomainDnsConsumer.WaitTime)

	mux := http.NewServeMux()
	mux.HandleFunc("/", xhttp.MSRoute)
	mux.Handle("/metrics", promhttp.Handler())

	// Add the pprof routes
	// mux.HandleFunc("/debug/pprof/", pprof.Index)
	// mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	// mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	// mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	// mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	//
	// mux.Handle("/debug/pprof/block", pprof.Handler("block"))
	// mux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	// mux.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	// mux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	gp.Process()
	tg.Process()
	hs.Process()
	domainDnsCons.Consume(context.Background())
	log.Logger.Info(context.Background(), "main: service is up")
	log.Logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", c.HTTP.Port), mux))
}

func initialize() (*Config, error) {
	c := &Config{}
	err := envconfig.Process(serviceName, c)
	if err != nil {
		return nil, err
	}
	return c, nil
}
