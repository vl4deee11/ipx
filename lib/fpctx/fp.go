package fpctx

import (
	"context"
	"github.com/google/uuid"
	"time"
)

const timeout = 60 * time.Second

func APICtx() context.Context {
	ctx, _ := context.WithTimeout(context.Background(), timeout)
	return context.WithValue(ctx, "fp", uuid.NewString())
}

func WorkerCtx() (context.Context, context.CancelFunc) {
	ctx, c := context.WithTimeout(context.Background(), timeout)
	return context.WithValue(ctx, "fp", uuid.NewString()), c
}

func WorkerCtxEndless() context.Context {
	return context.WithValue(context.Background(), "fp", uuid.NewString())
}
