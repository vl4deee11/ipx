package xhttp

import (
	"net/http"
	"time"
)

type WriterWithStatusCode struct {
	http.ResponseWriter
	StatusCode int
	Start      time.Time
	Route      string
}

func NewWriter(w http.ResponseWriter, route string) *WriterWithStatusCode {
	return &WriterWithStatusCode{w, http.StatusOK, time.Now(), route}
}

func (w *WriterWithStatusCode) WriteHeader(code int) {
	w.StatusCode = code
	w.ResponseWriter.WriteHeader(code)
}
