package xhttp

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/vl4deee11/ipx/lib/cache"
	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/lib/metrics"
)

const badRequest = "Bad request"
const iseRequest = "Internal server error"
const unauthorized = "Unauthorized"
const notFound = "Not found"

type HTTPResponse struct {
	err  string
	data interface{}
	RW   *WriterWithStatusCode
	Ctx  context.Context
}

func (hr *HTTPResponse) Marshal() ([]byte, error) {
	return json.Marshal(map[string]interface{}{
		"data":  hr.data,
		"error": hr.err,
	})
}

func (hr *HTTPResponse) NotFound() {
	hr.err = notFound
	hr.RW.WriteHeader(http.StatusNotFound)
	hr.write()
	metrics.ObserveXHTTP(hr.RW.Start, hr.RW.Route, http.StatusNotFound)
}

func (hr *HTTPResponse) Unauthorized() {
	hr.err = unauthorized
	hr.RW.WriteHeader(http.StatusUnauthorized)
	hr.write()
	metrics.ObserveXHTTP(hr.RW.Start, hr.RW.Route, http.StatusUnauthorized)
}

func (hr *HTTPResponse) OK(d interface{}) {
	hr.data = d
	hr.RW.WriteHeader(http.StatusOK)
	hr.write()
	metrics.ObserveXHTTP(hr.RW.Start, hr.RW.Route, http.StatusOK)
}

func (hr *HTTPResponse) BadRequest() {
	hr.err = badRequest
	hr.RW.WriteHeader(http.StatusBadRequest)
	hr.write()
	metrics.ObserveXHTTP(hr.RW.Start, hr.RW.Route, http.StatusBadRequest)
}

func (hr *HTTPResponse) ISE(err error) {
	log.Logger.Errorf(hr.Ctx, "xhttp: error on api. ERR => %s \n", err.Error())
	hr.err = iseRequest
	hr.RW.WriteHeader(http.StatusInternalServerError)
	hr.write()
	metrics.ObserveXHTTP(hr.RW.Start, hr.RW.Route, http.StatusInternalServerError)
}

func (hr *HTTPResponse) write() {
	b, err := hr.Marshal()
	if err != nil {
		log.Logger.Errorf(hr.Ctx, "xhttp: error in marshaling results. ERR => %s \n", err.Error())
		_, _ = hr.RW.Write([]byte{})
		return
	}
	_, err = hr.RW.Write(b)
	if err != nil {
		log.Logger.Errorf(hr.Ctx, "xhttp: error in writing results to http.ResponseWriter. ERR => %s \n", err.Error())
	}
}

func (hr *HTTPResponse) CheckAccess(r *http.Request) bool {
	return hr.checkAccessByQ(r, "t")
}

func (hr *HTTPResponse) checkAccessByQ(re *http.Request, q string) bool {
	t := re.URL.Query().Get(q)
	r, err := cache.Cli.Get(hr.Ctx, t)
	if err != nil {
		log.Logger.Errorf(hr.Ctx, "xhttp: error on api. ERR => %s \n", err.Error())
		hr.Unauthorized()
		return false
	}
	if r == t {
		return true
	}

	hr.Unauthorized()
	return false
}

func MSRoute(w http.ResponseWriter, r *http.Request) {
	lw := NewWriter(w, r.URL.Path)
	ctx := fpctx.APICtx()
	switch {
	case r.URL.Path == "/health" || r.URL.Path == "/ready":
		lw.WriteHeader(http.StatusOK)
		_, _ = fmt.Fprintf(lw, "ok")

	default:
		lw.WriteHeader(http.StatusNotFound)
		_, _ = fmt.Fprintf(lw, "404 not found")
	}
	log.Logger.Infof(
		ctx,
		"api: %s %s%s [%d]",
		r.Method,
		r.Host,
		r.URL.String(),
		lw.StatusCode,
	)
}
