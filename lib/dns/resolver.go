package dns

import (
	"context"
	"errors"
	"net"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/common"
	"gitlab.com/vl4deee11/ipx/lib/log"
)

type Resolver struct {
	r *net.Resolver
}

var Res *Resolver

func NewResolver(dns string) error {
	if dns == "" {
		return errors.New("empty dns address")
	}
	tenSecInMs := 10000
	Res = &Resolver{
		r: &net.Resolver{
			PreferGo: true,
			Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
				d := net.Dialer{Timeout: time.Millisecond * time.Duration(tenSecInMs)}
				return d.DialContext(ctx, "udp", dns)
			},
		},
	}
	return nil
}

func (rr *Resolver) Resolve(ctx context.Context, domains []string) map[string][]string {
	log.Logger.Debug(ctx, "dns: starting resolve new buffer domains")
	res := map[string][]string{}
	for i := 0; i < len(domains); i++ {
		ips, _ := rr.r.LookupHost(ctx, domains[i])
		if len(ips) > 0 {
			res[domains[i]] = ips
		}
	}
	log.Logger.Debugf(ctx, "dns: %d domains are resolved", len(res))
	return res
}

func (rr *Resolver) ResolveFromCert(ctx context.Context, buff []*common.Cert) map[string][]string {
	log.Logger.Debug(ctx, "dns: starting resolve new buffer of certificated")
	res := map[string][]string{}
	for i := 0; i < len(buff); i++ {
		if buff[i] == nil {
			continue
		}
		for j := 0; j < len(buff[i].LeafCert.AllDomains); j++ {
			domain := buff[i].LeafCert.AllDomains[j]
			ips, _ := rr.r.LookupHost(ctx, domain)
			if len(ips) > 0 {
				res[domain] = ips
			}
		}
	}
	log.Logger.Debugf(ctx, "dns: %d domains are resolved", len(res))
	return res
}
