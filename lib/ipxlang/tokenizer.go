package ipxlang

import (
	"gitlab.com/vl4deee11/ipx/lib/log"
	"golang.org/x/net/context"

	"github.com/timtadh/lexmachine"
	"github.com/timtadh/lexmachine/machines"
)

type tokenizer struct {
	lexer *lexmachine.Lexer
}

func newTokenizer() (*tokenizer, error) {
	t := new(tokenizer)
	l := lexmachine.NewLexer()

	// Boolean ops
	l.Add([]byte(`\or`), t.newToken(OR))
	l.Add([]byte(`\and`), t.newToken(AND))
	l.Add([]byte(`\!`), t.newToken(NOT))

	// Array ops
	l.Add([]byte(`\in`), t.newToken(IN))
	l.Add([]byte(`\all:in`), t.newToken(ALLIN))
	l.Add([]byte(`\!:in`), t.newToken(NOTIN))

	// Compare ops
	l.Add([]byte(`\~`), t.newToken(LIKE))
	l.Add([]byte(`\=`), t.newToken(EQ))
	l.Add([]byte(`\!=`), t.newToken(NEQ))
	l.Add([]byte(`\>`), t.newToken(GT))
	l.Add([]byte(`\<`), t.newToken(LT))
	l.Add([]byte(`\>=`), t.newToken(GE))
	l.Add([]byte(`\<=`), t.newToken(LE))

	// Get all op
	l.Add([]byte(`\all`), t.newToken(ALL))

	// Pairs
	l.Add([]byte(`\(`), t.newToken(OPENPAIR))
	l.Add([]byte(`\)`), t.newToken(CLOSEPAIR))

	// Identifier
	l.Add([]byte(`[a-zA-Z_][a-zA-Z0-9_]*`), t.newToken(ID))

	// Types
	l.Add([]byte(`[0-9]*\.?[0-9]+`), t.newToken(INT))
	l.Add([]byte(`'[a-zA-Z0-9.?:]*?'`), t.newToken(STR))

	// Skip
	l.Add([]byte(`( |\t|\f)+`), t.skip)
	l.Add([]byte(`\,`), t.skip)

	if err := l.Compile(); err != nil {
		return nil, err
	}

	t.lexer = l
	return t, nil
}

func (t *tokenizer) skip(_ *lexmachine.Scanner, _ *machines.Match) (interface{}, error) {
	return nil, nil
}

func (t *tokenizer) newToken(tt int) lexmachine.Action {
	return func(s *lexmachine.Scanner, m *machines.Match) (interface{}, error) {
		return s.Token(tt, string(m.Bytes), m), nil
	}
}

func (t *tokenizer) getTokens(ctx context.Context, raw string) ([]*lexmachine.Token, error) {
	scanner, err := t.lexer.Scanner([]byte(raw))
	if err != nil {
		return nil, err
	}

	tokens := make([]*lexmachine.Token, 0)
	for tk, err, eof := scanner.Next(); !eof; tk, err, eof = scanner.Next() {
		if err != nil {
			return nil, err
		}
		tkT := tk.(*lexmachine.Token)
		tokens = append(tokens, tkT)
		log.Logger.Debugf(
			ctx,
			"%-7v | %-25q | %v:%v-%v:%v\n",
			tokenToStr(tkT.Type),
			tkT.Value,
			tkT.StartLine,
			tkT.StartColumn,
			tkT.EndLine,
			tkT.EndColumn,
		)
	}

	return tokens, nil
}
