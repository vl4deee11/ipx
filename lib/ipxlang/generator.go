package ipxlang

import (
	"fmt"
	"strings"
)

type generator struct{}

func (g *generator) clearColName(col interface{}) string {
	return col.(string)
}

//
// Boolean ops
func (g *generator) generateOR(expr1, expr2 interface{}) string {
	return fmt.Sprintf("(%s) or (%s)", expr1, expr2)
}

func (g *generator) generateAND(expr1, expr2 interface{}) string {
	return fmt.Sprintf("(%s) and (%s)", expr1, expr2)
}

func (g *generator) generateNOT(expr interface{}) string {
	return fmt.Sprintf("not(%s)", expr)
}

//
// Array ops
func (g *generator) generateIN(val, col interface{}) string {
	return fmt.Sprintf("has(%s, %s)", g.clearColName(col), val)
}

func (g *generator) generateALLIN(val, col interface{}) string {
	return fmt.Sprintf("hasAll([%s], %s)", val, g.clearColName(col))
}

func (g *generator) generateNOTIN(val, col interface{}) string {
	return fmt.Sprintf("not(has(%s, %s))", g.clearColName(col), val)
}

//
// Compare ops
func (g *generator) generateLIKE(val, col interface{}) string {
	val = strings.ReplaceAll(val.(string), "?", "%")
	return fmt.Sprintf("ilike(%s, %s)", g.clearColName(col), val)
}

func (g *generator) generateEQ(val, col interface{}) string {
	return fmt.Sprintf("%s = %s", g.clearColName(col), val)
}

func (g *generator) generateNEQ(val, col interface{}) string {
	return fmt.Sprintf("%s != %s", g.clearColName(col), val)
}

func (g *generator) generateGT(val, col interface{}) string {
	return fmt.Sprintf("%s > %s", g.clearColName(col), val)
}

func (g *generator) generateLT(val, col interface{}) string {
	return fmt.Sprintf("%s < %s", g.clearColName(col), val)
}

func (g *generator) generateGE(val, col interface{}) string {
	return fmt.Sprintf("%s >= %s", g.clearColName(col), val)
}

func (g *generator) generateLE(val, col interface{}) string {
	return fmt.Sprintf("%s <= %s", g.clearColName(col), val)
}

//
// Func call op
func (g *generator) generateFUNC(fn interface{}, args []interface{}) string {
	strArgs := make([]string, len(args))
	for i := range args {
		strArgs[i] = args[i].(string)
	}
	return fmt.Sprintf("%s(%s)", fn, strings.Join(strArgs, ","))
}

//
// Get all op
func (g *generator) generateALL() string {
	return "(1)"
}
