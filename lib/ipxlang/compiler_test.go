package ipxlang

import (
	"context"
	"fmt"
	"testing"

	"gitlab.com/vl4deee11/ipx/lib/log"

	"github.com/stretchr/testify/assert"
)

type TestCompiled struct {
	actual   string
	expected string
}

//nolint:funlen // big test for different compiled expr
func TestCompiler_CompileSimpleExpr(t *testing.T) {
	log.MakeLogger("info")
	err := NewCompiler()
	assert.Equal(t, nil, err)

	tests := []TestCompiled{
		{
			actual:   "all",
			expected: "(1)",
		},
		{
			actual:   "toDate('str') > 0",
			expected: "toDate('str') > 0",
		},
		{
			actual:   "toDate('str',1) ~ '234'",
			expected: "ilike(toDate('str',1), '234')",
		},
		{
			actual:   "toDateTime('str',     1,   '231') > 0",
			expected: "toDateTime('str',1,'231') > 0",
		},

		{
			actual:   "toDateTime('str',     1,   '231') > toDate('str')",
			expected: "toDateTime('str',1,'231') > toDate('str')",
		},

		{
			actual:   "some_field > toDate('str')",
			expected: "some_field > toDate('str')",
		},

		{
			actual:   "toDate('str') > some_field",
			expected: "toDate('str') > some_field",
		},

		{
			actual:   "some_field > ip",
			expected: "some_field > ip",
		},
		{
			actual:   "some_field < ip",
			expected: "some_field < ip",
		},
		{
			actual:   "some_field >= ip",
			expected: "some_field >= ip",
		},
		{
			actual:   "some_field <= ip",
			expected: "some_field <= ip",
		},

		{
			actual:   "some_field > 'str'",
			expected: "some_field > 'str'",
		},
		{
			actual:   "some_field < 'str'",
			expected: "some_field < 'str'",
		},
		{
			actual:   "some_field >= 'str'",
			expected: "some_field >= 'str'",
		},
		{
			actual:   "some_field <= 'str'",
			expected: "some_field <= 'str'",
		},
		{
			actual:   "all",
			expected: "(1)",
		},
		{
			actual:   "(!(((all)))) and !some_field ~ 'ab?cd.com'",
			expected: "(not((1))) and (not(ilike(some_field, 'ab%cd.com')))",
		},
		{
			actual:   "some_field ~ 'ab?cd.com'",
			expected: "ilike(some_field, 'ab%cd.com')",
		},
		{
			actual:   "some_field = 'abcd.com'",
			expected: "some_field = 'abcd.com'",
		},
		{
			actual:   "some_field = ip",
			expected: "some_field = ip",
		},

		{
			actual:   "some_field != 'abcd.com'",
			expected: "some_field != 'abcd.com'",
		},
		{
			actual:   "some_field != ip",
			expected: "some_field != ip",
		},

		{
			actual:   "'abcd.com' in v_1_some_field",
			expected: "has(v_1_some_field, 'abcd.com')",
		},
		{
			actual:   "123 in some_field",
			expected: "has(some_field, 123)",
		},
		{
			actual:   "'123.123.123.1' all:in ips",
			expected: "hasAll(['123.123.123.1'], ips)",
		},
		{
			actual:   "11 all:in ips",
			expected: "hasAll([11], ips)",
		},
		{
			actual:   "(11 all:in ips) or (some_field ~ 'ab?cd.com')",
			expected: "(hasAll([11], ips)) or (ilike(some_field, 'ab%cd.com'))",
		},
		{
			actual:   "(11 all:in ips) and (some_field ~ 'ab?cd.com')",
			expected: "(hasAll([11], ips)) and (ilike(some_field, 'ab%cd.com'))",
		},
		{
			actual:   "(!(11 all:in ips)) and (some_field ~ 'ab?cd.com')",
			expected: "(not(hasAll([11], ips))) and (ilike(some_field, 'ab%cd.com'))",
		},
		{
			actual:   "!(11 all:in ips)",
			expected: "not(hasAll([11], ips))",
		},
		{
			actual:   "(11 all:in ips) or (some_field = 'ad.com')",
			expected: "(hasAll([11], ips)) or (some_field = 'ad.com')",
		},
		{
			actual:   "(11 all:in ips) and (some_field = 'abcd.com')",
			expected: "(hasAll([11], ips)) and (some_field = 'abcd.com')",
		},
		{
			actual:   "(!(11 all:in ips)) and (some_field = 123)",
			expected: "(not(hasAll([11], ips))) and (some_field = 123)",
		},
		{
			actual:   "(!(11 all:in ips)) and (length(ips) > 0)",
			expected: "(not(hasAll([11], ips))) and (length(ips) > 0)",
		},
	}
	for i := range tests {
		r, err := Compiler.Compile(context.Background(), tests[i].actual)
		assert.Equal(t, nil, err)
		assert.Equal(t, tests[i].expected, r)
	}
}

func TestCompiler_CompileBooleanOp(t *testing.T) {
	log.MakeLogger("info")
	err := NewCompiler()
	assert.Equal(t, nil, err)

	tests := []TestCompiled{
		{
			actual:   "some_field = 'string.:string' and some_field = 'string.:string'",
			expected: "(some_field = 'string.:string') and (some_field = 'string.:string')",
		},
		{
			actual:   "some_field = 'string.:string' or some_field = 'string.:string'",
			expected: "(some_field = 'string.:string') or (some_field = 'string.:string')",
		},
		{
			actual:   "!some_field = 'string.:string' and some_field = 'string.:string'",
			expected: "not((some_field = 'string.:string') and (some_field = 'string.:string'))",
		},
		{
			actual:   "!some_field = 'string.:string' or some_field = 'string.:string'",
			expected: "not((some_field = 'string.:string') or (some_field = 'string.:string'))",
		},
		{
			actual:   "!some_field = 'string.:string' or !some_field = 'string.:string'",
			expected: "not((some_field = 'string.:string') or (not(some_field = 'string.:string')))",
		},
		{
			actual:   "(!some_field = 'string.:string') or some_field = 'string.:string'",
			expected: "(not(some_field = 'string.:string')) or (some_field = 'string.:string')",
		},
		{
			actual:   "some_field = 'string.:string' and some_field = 'string.:string' and some_field = 'string.:string'",
			expected: "(some_field = 'string.:string') and ((some_field = 'string.:string') and (some_field = 'string.:string'))",
		},
		{
			actual:   "(some_field = 'string.:string' and some_field = 'string.:string') and (some_field = 'string.:string')",
			expected: "((some_field = 'string.:string') and (some_field = 'string.:string')) and (some_field = 'string.:string')",
		},
		{
			actual:   "some_field = 'string.:string' or some_field = 'string.:string' and some_field = 'string.:string'",
			expected: "(some_field = 'string.:string') or ((some_field = 'string.:string') and (some_field = 'string.:string'))",
		},
		{
			actual:   "some_field = 'string.:string' or some_field = 'string.:string' or some_field = 'string.:string'",
			expected: "(some_field = 'string.:string') or ((some_field = 'string.:string') or (some_field = 'string.:string'))",
		},
		{
			actual:   "(('123' all:in some3_field) or (123 in some2_field)) and some_field = 'string.:string'",
			expected: "((hasAll(['123'], some3_field)) or (has(some2_field, 123))) and (some_field = 'string.:string')",
		},
	}
	for i := range tests {
		r, err := Compiler.Compile(context.Background(), tests[i].actual)
		assert.Equal(t, nil, err)
		assert.Equal(t, tests[i].expected, r)
	}
}

func TestCompiler_CompileCompareOp(t *testing.T) {
	log.MakeLogger("info")
	err := NewCompiler()
	assert.Equal(t, nil, err)

	comp := []string{"=", ">", "<", ">=", "<=", "!="}

	tests := []TestCompiled{}

	for i := range comp {
		tests = append(tests,
			TestCompiled{
				actual:   fmt.Sprintf("some_field %s 'string.:string'", comp[i]),
				expected: fmt.Sprintf("some_field %s 'string.:string'", comp[i]),
			},
			TestCompiled{
				actual:   fmt.Sprintf("some_field %s some2_field", comp[i]),
				expected: fmt.Sprintf("some_field %s some2_field", comp[i]),
			},
			TestCompiled{
				actual:   fmt.Sprintf("some_field %s 123", comp[i]),
				expected: fmt.Sprintf("some_field %s 123", comp[i]),
			},
			TestCompiled{
				actual:   fmt.Sprintf("some_func(ip,'123',123) %s 'string.:string'", comp[i]),
				expected: fmt.Sprintf("some_func(ip,'123',123) %s 'string.:string'", comp[i]),
			},
			TestCompiled{
				actual:   fmt.Sprintf("some_func(ip,'123',123) %s 123", comp[i]),
				expected: fmt.Sprintf("some_func(ip,'123',123) %s 123", comp[i]),
			},
			TestCompiled{
				actual:   fmt.Sprintf("some_field %s some_func(ip,'123',123)", comp[i]),
				expected: fmt.Sprintf("some_field %s some_func(ip,'123',123)", comp[i]),
			},
			TestCompiled{
				actual:   fmt.Sprintf("some_func(ip,'123',123) %s some_field", comp[i]),
				expected: fmt.Sprintf("some_func(ip,'123',123) %s some_field", comp[i]),
			},
			TestCompiled{
				actual:   fmt.Sprintf("some_func2(ip,'123',123) %s some_func(ip,'123',123)", comp[i]),
				expected: fmt.Sprintf("some_func2(ip,'123',123) %s some_func(ip,'123',123)", comp[i]),
			},
		)
	}

	tests = append(tests,
		TestCompiled{
			actual:   "some_func(ip,'123',123) ~ 'd?m?n'",
			expected: "ilike(some_func(ip,'123',123), 'd%m%n')",
		},
		TestCompiled{
			actual:   "some_field ~ 'do?m?in'",
			expected: "ilike(some_field, 'do%m%in')",
		},
	)

	for i := range tests {
		r, err := Compiler.Compile(context.Background(), tests[i].actual)
		assert.Equal(t, nil, err)
		assert.Equal(t, tests[i].expected, r)
	}
}

func TestCompiler_CompileArrayOp(t *testing.T) {
	log.MakeLogger("info")
	err := NewCompiler()
	assert.Equal(t, nil, err)

	tests := []TestCompiled{
		{
			actual:   "'string' in some_field",
			expected: "has(some_field, 'string')",
		},
		{
			actual:   "123 in some_field",
			expected: "has(some_field, 123)",
		},
		{
			actual:   "'string' !:in some_field",
			expected: "not(has(some_field, 'string'))",
		},
		{
			actual:   "123 !:in some_field",
			expected: "not(has(some_field, 123))",
		},
		{
			actual:   "'string' all:in some_field",
			expected: "hasAll(['string'], some_field)",
		},
		{
			actual:   "123 all:in some_field",
			expected: "hasAll([123], some_field)",
		},
		{
			actual:   "'string' in some_func(1,'12', some_id_param)",
			expected: "has(some_func(1,'12',some_id_param), 'string')",
		},
		{
			actual:   "123 in some_func(1,'12', some_id_param)",
			expected: "has(some_func(1,'12',some_id_param), 123)",
		},
		{
			actual:   "'string' !:in some_func(1,'12', some_id_param)",
			expected: "not(has(some_func(1,'12',some_id_param), 'string'))",
		},
		{
			actual:   "123 !:in some_func(1,'12', some_id_param)",
			expected: "not(has(some_func(1,'12',some_id_param), 123))",
		},
		{
			actual:   "'string' all:in some_func(1,'12', some_id_param)",
			expected: "hasAll(['string'], some_func(1,'12',some_id_param))",
		},
		{
			actual:   "123 all:in some_func(1,'12', some_id_param)",
			expected: "hasAll([123], some_func(1,'12',some_id_param))",
		},
		{
			actual:   "123 all:in some_func()",
			expected: "hasAll([123], some_func())",
		},
	}
	for i := range tests {
		r, err := Compiler.Compile(context.Background(), tests[i].actual)
		assert.Equal(t, nil, err)
		assert.Equal(t, tests[i].expected, r)
	}
}
