package ipxlang

const (
	// Boolean ops
	OR int = iota
	AND
	NOT

	// Array ops
	IN
	ALLIN
	NOTIN

	// Compare ops
	LIKE
	EQ
	NEQ
	GT
	LT
	GE
	LE

	// Get all op
	ALL

	// Pairs
	OPENPAIR
	CLOSEPAIR

	// Identifier
	ID

	// Types
	STR
	INT
)

//nolint:gocyclo // special logic for tokens
func tokenToStr(t int) string {
	switch t {
	case OR:
		return "or"
	case AND:
		return "and"
	case IN:
		return "in"
	case ALLIN:
		return "all:in"
	case NOTIN:
		return "!:in"
	case ALL:
		return "all"
	case NOT:
		return "!"
	case OPENPAIR:
		return "("
	case CLOSEPAIR:
		return ")"
	case STR:
		return "'string'"
	case INT:
		return "(0-9)[*] number"
	case LIKE:
		return "~"
	case EQ:
		return "="
	case NEQ:
		return "!="
	case GT:
		return ">"
	case LT:
		return "<"
	case GE:
		return ">="
	case LE:
		return "<="
	case ID:
		return "identifier"
	default:
		return "???"
	}
}
