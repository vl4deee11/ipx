# IPX language

**Created to simplify working with data and the clickhouse database**

# Grammar

```md

EXPR -> SUB_EXPR | SUB_EXPR 'or' EXPR | SUB_EXPR 'and' EXPR

SUB_EXPR -> '(' EXPR ')' | NOT | ARRAY_OP | COMPARE_OP | ALL

NOT -> '!' EXPR

ARRAY_OP -> IN | ALLIN | NOTIN

IN -> TYPE 'in' ID | TYPE 'in' FUNC

ALLIN -> TYPE 'all:in' ID | TYPE 'all:in' FUNC

NOTIN -> TYPE 'not:in' ID | TYPE 'not:in' FUNC

COMPARE_OP -> LIKE | EQ | NEQ | GT | LT | GE | LE

LIKE -> ID '~' STR | FUNC '~' STR

NEQ -> ID '!=' TYPE | ID '!=' ID | ID '!=' FUNC | FUNC '!=' FUNC | FUNC '!=' ID | FUNC '!=' TYPE

EQ -> ID '=' TYPE | ID '=' ID | ID '=' FUNC | FUNC '=' FUNC | FUNC '=' ID | FUNC '=' TYPE

GT -> ID '>' TYPE | ID '>' ID | ID '>' FUNC | FUNC '>' FUNC | FUNC '>' ID | FUNC '>' TYPE

LT -> ID '<' TYPE | ID '<' ID | ID '<' FUNC | FUNC '<' FUNC | FUNC '<' ID | FUNC '<' TYPE

GE -> ID '>=' TYPE | ID '>=' ID | ID '>=' FUNC | FUNC '>=' FUNC | FUNC '>=' ID | FUNC '>=' TYPE

LE -> ID '<=' TYPE | ID '<=' ID | ID '<=' FUNC | FUNC '<=' FUNC | FUNC '<=' ID | FUNC '<=' TYPE

ALL -> 'all'

FUNC -> ID '(' TYPE | ID, ... ')'

ID -> [a-zA-Z_][a-zA-Z0-9_]*

INT -> [0-9]*\.?[0-9]+

STR -> '[a-zA-Z0-9.?:]*?'

TYPE -> STR | INT
```


