package ipxlang

import (
	"errors"
	"fmt"
	"strings"

	"github.com/timtadh/lexmachine"
)

type parser struct {
	tokens  []*lexmachine.Token
	pointer int
	gen     *generator
}

func newParser(tokens []*lexmachine.Token) (*parser, error) {
	if len(tokens) == 0 {
		return nil, errors.New("empty list of tokens")
	}

	return &parser{
		tokens:  tokens,
		pointer: 0,
		gen:     &generator{},
	}, nil
}

//
// Help methods
func (p *parser) peek() (*lexmachine.Token, error) {
	if p.pointer >= len(p.tokens) {
		return nil, errors.New("unexpected end of expression")
	}
	return p.tokens[p.pointer], nil
}

func (p *parser) isType(t int) bool {
	return t == INT || t == STR
}

func (p *parser) expectedErr(types ...int) error {
	strTypes := make([]string, len(types)-1)
	for i := 0; i < len(types)-1; i++ {
		strTypes[i] = tokenToStr(types[i])
	}
	return fmt.Errorf("%s expected but got %s", strings.Join(strTypes, ","), tokenToStr(types[len(types)-1]))
}

func (p *parser) incr() {
	p.pointer++
}

//
// Parse expression
func (p *parser) parse() (string, error) {
	r, err := p.parseEXPR()
	if err != nil {
		return "", err
	}

	if p.pointer != len(p.tokens) {
		return "", fmt.Errorf("error in expression at %s", p.tokens[p.pointer].Value)
	}

	return r, nil
}

func (p *parser) parseEXPR() (string, error) {
	r, err := p.parseSUBEXPR()
	if err != nil {
		return "", err
	}

	if p.pointer == len(p.tokens) {
		return r, nil
	}

	next := []func(interface{}) (string, error){p.parseOR, p.parseAND}
	for i := range next {
		if nextR, err := next[i](r); err != nil {
			return "", err
		} else if nextR != "" {
			return nextR, nil
		}
	}

	return r, nil
}

func (p *parser) parseSUBEXPR() (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == OPENPAIR {
		p.incr()
		r, err := p.parseEXPR()
		if err != nil {
			return "", err
		}

		tk, err = p.peek()
		if err != nil {
			return "", err
		}

		if tk.Type != CLOSEPAIR {
			return "", p.expectedErr(CLOSEPAIR, tk.Type)
		}

		p.incr()
		return r, nil
	}

	next := []func() (string, error){
		p.parseArrayOp, p.parseCompareOp, p.parseNOT, p.parseALL,
	}

	for i := range next {
		if nextR, err := next[i](); err != nil {
			return "", err
		} else if nextR != "" {
			return nextR, nil
		}
	}

	return "", nil
}

//
// Boolean ops
func (p *parser) parseOR(s interface{}) (string, error) {
	return p.parseBinaryOpT(s, OR, p.gen.generateOR)
}

func (p *parser) parseAND(s interface{}) (string, error) {
	return p.parseBinaryOpT(s, AND, p.gen.generateAND)
}

func (p *parser) parseNOT() (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == NOT {
		p.incr()
		r, err := p.parseEXPR()
		if err != nil {
			return "", err
		}
		return p.gen.generateNOT(r), nil
	}

	return "", nil
}

//
// Array ops
func (p *parser) parseArrayOp() (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if p.isType(tk.Type) {
		val := tk.Value
		p.incr()

		next := []func(interface{}) (string, error){p.parseIN, p.parseALLIN, p.parseNOTIN}
		for i := range next {
			if nextR, err := next[i](val); err != nil {
				return "", err
			} else if nextR != "" {
				return nextR, nil
			}
		}
	}
	return "", nil
}

func (p *parser) parseIN(val interface{}) (string, error) {
	return p.parseArrayOpT(val, IN, p.gen.generateIN)
}

func (p *parser) parseALLIN(val interface{}) (string, error) {
	return p.parseArrayOpT(val, ALLIN, p.gen.generateALLIN)
}

func (p *parser) parseNOTIN(val interface{}) (string, error) {
	return p.parseArrayOpT(val, NOTIN, p.gen.generateNOTIN)
}

//
// Compare ops
func (p *parser) parseCompareOp() (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == ID {
		id := tk.Value
		p.incr()

		r, err := p.parseFUNC(id)
		if err != nil {
			return "", err
		} else if r != "" {
			id = r
		}

		next := []func(interface{}) (string, error){
			p.parseLIKE, p.parseEQ, p.parseNEQ, p.parseGT, p.parseLT, p.parseGE, p.parseLE,
		}
		for i := range next {
			if nextR, err := next[i](id); err != nil {
				return "", err
			} else if nextR != "" {
				return nextR, nil
			}
		}
	}

	return "", nil
}

func (p *parser) parseLIKE(name interface{}) (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == LIKE {
		p.incr()
		tk, err = p.peek()
		if err != nil {
			return "", err
		}

		if tk.Type != STR {
			return "", p.expectedErr(STR, tk.Type)
		}

		val := tk.Value
		p.incr()
		return p.gen.generateLIKE(val, name), nil
	}

	return "", nil
}

func (p *parser) parseEQ(name interface{}) (string, error) {
	return p.parseCompareOpT(name, EQ, p.gen.generateEQ)
}

func (p *parser) parseNEQ(name interface{}) (string, error) {
	return p.parseCompareOpT(name, NEQ, p.gen.generateNEQ)
}

func (p *parser) parseGT(name interface{}) (string, error) {
	return p.parseCompareOpT(name, GT, p.gen.generateGT)
}

func (p *parser) parseLT(name interface{}) (string, error) {
	return p.parseCompareOpT(name, LT, p.gen.generateLT)
}

func (p *parser) parseGE(name interface{}) (string, error) {
	return p.parseCompareOpT(name, GE, p.gen.generateGE)
}

func (p *parser) parseLE(name interface{}) (string, error) {
	return p.parseCompareOpT(name, LE, p.gen.generateLE)
}

//
// Func call op
func (p *parser) parseFUNC(name interface{}) (string, error) {
	if p.pointer == len(p.tokens) {
		return "", nil
	}

	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type != OPENPAIR {
		return "", nil
	}

	p.incr()
	args := make([]interface{}, 0)
	for {
		tk, err := p.peek()
		if err != nil {
			return "", err
		}
		p.incr()

		if tk.Type == CLOSEPAIR {
			break
		} else if p.isType(tk.Type) || tk.Type == ID {
			args = append(args, tk.Value)
		} else {
			return "", p.expectedErr(ID, OPENPAIR, STR, INT, ID, CLOSEPAIR, tk.Type)
		}
	}

	return p.gen.generateFUNC(name, args), nil
}

//
// Get all op
func (p *parser) parseALL() (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == ALL {
		p.incr()
		return p.gen.generateALL(), nil
	}

	return "", nil
}

//
// Template functions
func (p *parser) parseCompareOpT(s interface{}, t int, gen func(interface{}, interface{}) string) (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == t {
		p.incr()
		tk, err = p.peek()
		if err != nil {
			return "", err
		}

		val := tk.Value
		if tk.Type == ID {
			p.incr()

			r, err := p.parseFUNC(val)
			if err != nil {
				return "", err
			} else if r != "" {
				val = r
			}
		} else if p.isType(tk.Type) {
			p.incr()
		} else {
			return "", p.expectedErr(ID, STR, INT, tk.Type)
		}

		return gen(val, s), nil
	}

	return "", nil
}

func (p *parser) parseBinaryOpT(s interface{}, t int, gen func(interface{}, interface{}) string) (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == t {
		p.incr()
		r, err := p.parseEXPR()
		if err != nil {
			return "", err
		}
		return gen(s, r), nil
	}

	return "", nil
}

func (p *parser) parseArrayOpT(val interface{}, t int, gen func(interface{}, interface{}) string) (string, error) {
	tk, err := p.peek()
	if err != nil {
		return "", err
	}

	if tk.Type == t {
		p.incr()
		tk, err = p.peek()
		if err != nil {
			return "", err
		}

		id := tk.Value
		if tk.Type != ID {
			return "", p.expectedErr(ID, tk.Type)
		}

		p.incr()

		r, err := p.parseFUNC(id)
		if err != nil {
			return "", err
		} else if r != "" {
			id = r
		}

		return gen(val, id), nil
	}

	return "", nil
}
