package ipxlang

import (
	"context"

	"gitlab.com/vl4deee11/ipx/lib/log"
)

var Compiler *Comp

type Comp struct {
	tokenizer *tokenizer
}

func NewCompiler() error {
	t, err := newTokenizer()
	if err != nil {
		return err
	}

	Compiler = &Comp{tokenizer: t}
	return nil
}

func (c *Comp) Compile(ctx context.Context, s string) (string, error) {
	tokens, err := c.tokenizer.getTokens(ctx, s)
	if err != nil {
		log.Logger.Warnf(ctx, "compiler: get tokens. ERR => %s\n", err.Error())
		return "", err
	}

	p, err := newParser(tokens)
	if err != nil {
		log.Logger.Warnf(ctx, "compiler: create parser. ERR => %s\n", err.Error())
		return "", err
	}

	r, err := p.parse()
	if err != nil {
		log.Logger.Warnf(ctx, "compiler: parse. ERR => %s\n", err.Error())
		return "", err
	}

	log.Logger.Debug(ctx, r)
	return r, nil
}
