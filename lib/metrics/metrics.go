package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"strconv"
	"time"
)

var (
	xHTTP = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "ipx_handler_requests_total",
		Help:    "Processed http requests",
		Buckets: []float64{5, 10, 50, 100, 200, 300, 500, 1000, 5000, 10000, 60000, 600000},
	}, []string{"route", "code"})
)

func ObserveXHTTP(start time.Time, route string, code int) {
	xHTTP.WithLabelValues(route, strconv.Itoa(code)).Observe(float64(time.Since(start).Milliseconds()))
}
