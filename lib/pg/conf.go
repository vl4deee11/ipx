package pg

import "time"

const timeoutPing = 15 * time.Second

type Conf struct {
	Host   string
	Port   uint16
	DBName string `split_words:"true" required:"true"`
	User   string `default:""`
	Pass   string `default:""`
}
