package pg

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"

	"time"

	"gitlab.com/vl4deee11/ipx/lib/log"

	// Register some standard stuff
	_ "github.com/lib/pq"
)

func Conn(host, user, pass, name string, port uint16) (*sqlx.DB, error) {
	db, err := sqlx.Open("postgres",
		fmt.Sprintf(
			"postgres://%s:%s@%s:%d/%s?sslmode=disable", user, pass, host, port, name),
	)
	if err != nil {
		return nil, err
	}

	ctxBg := context.Background()
	pingTry := 0
	for {
		ctx, cancel := context.WithTimeout(ctxBg, timeoutPing)
		err = db.PingContext(ctx)
		cancel()
		if err != nil {
			log.Logger.Errorf(ctx, "pg: try to ping. ERR => %s\n", err.Error())
			if time.Duration(pingTry)*time.Second > 1*time.Minute {
				// Dont sleep more than minute
				pingTry = 0
			}
			time.Sleep(time.Duration(pingTry) * time.Second)
			pingTry++
			continue
		}
		break
	}
	log.Logger.Debug(ctxBg, "pg: connected")
	return db, nil
}
