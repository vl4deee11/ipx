package common

type Cert struct {
	LeafCert ChainLeaf `json:"leaf_cert"`
}

type ChainLeaf struct {
	AllDomains []string `json:"all_domains"`
}
