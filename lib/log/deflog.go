package log

import (
	"bufio"
	"context"
	"fmt"
	"net"
	"os"
	"strings"
	"sync"
	"time"
	"unsafe"
)

const (
	trace = iota
	debug
	info
	warn
	err
)

const GLFmt = `{"version":"1.1","host":"ipx","message":"%v","level":%d}`

var lvl2str = [5]string{"TRACE", "DEBUG", "INFO", "WARN", "ERROR"}
var str2lvl = map[string]int{
	"debug": debug,
	"info":  info,
	"warn":  warn,
	"error": err,
	"trace": trace,
}

type LVLWrap struct {
	m       sync.Mutex
	w       *bufio.Writer
	l       int
	udpConn *net.UDPConn
}

func (l *LVLWrap) canPrint(lvl int) bool {
	return lvl >= l.l
}

func (l *LVLWrap) lvlFormat(ctx context.Context, lvl int, format string, v ...interface{}) {
	//fp, ok := ctx.Value("fp").(string)
	// ok := false
	now := time.Now().Format(time.RFC3339)
	var buf []byte
	// if ok {
	//	buf = make([]byte, 0, len(lvl2str[lvl])+len(format)+40+len(now)+2)
	// }
	buf = make([]byte, 0, len(lvl2str[lvl])+len(format)+3+len(now)+2)

	buf = append(buf, now...)
	buf = append(buf, 32)
	buf = append(buf, 91)
	buf = append(buf, lvl2str[lvl]...)
	buf = append(buf, 93, 32)
	// if ok {
	//	buf = append(buf, fp...)
	//	buf = append(buf, 32)
	// }
	buf = append(buf, format...)
	buf = append(buf, 10)
	msg := fmt.Sprintf(*(*string)(unsafe.Pointer(&buf)), v...)
	if l.udpConn != nil {
		l.udpConn.Write([]byte(fmt.Sprintf(GLFmt, msg, lvl)))
	}
	l.m.Lock()
	_, _ = l.w.WriteString(msg)
	_ = l.w.Flush()
	l.m.Unlock()
}

func (l *LVLWrap) Tracef(ctx context.Context, format string, v ...interface{}) {
	l.printf(ctx, trace, format, v...)
}

func (l *LVLWrap) Trace(ctx context.Context, v ...interface{}) {
	l.println(ctx, trace, v...)
}

func (l *LVLWrap) Debugf(ctx context.Context, format string, v ...interface{}) {
	l.printf(ctx, debug, format, v...)
}

func (l *LVLWrap) Debug(ctx context.Context, v ...interface{}) {
	l.println(ctx, debug, v...)
}

func (l *LVLWrap) Infof(ctx context.Context, format string, v ...interface{}) {
	l.printf(ctx, info, format, v...)
}

func (l *LVLWrap) Info(ctx context.Context, v ...interface{}) {
	l.println(ctx, info, v...)
}

func (l *LVLWrap) Warnf(ctx context.Context, format string, v ...interface{}) {
	l.printf(ctx, warn, format, v...)
}

func (l *LVLWrap) Warn(ctx context.Context, v ...interface{}) {
	l.println(ctx, warn, v...)
}

func (l *LVLWrap) Errorf(ctx context.Context, format string, v ...interface{}) {
	l.printf(ctx, err, format, v...)
}

func (l *LVLWrap) Error(ctx context.Context, v ...interface{}) {
	l.println(ctx, err, v...)
}

func (l *LVLWrap) printf(ctx context.Context, lvl int, format string, v ...interface{}) {
	if !l.canPrint(lvl) {
		return
	}
	l.lvlFormat(ctx, lvl, format, v...)
}

func (l *LVLWrap) println(ctx context.Context, lvl int, v ...interface{}) {
	if !l.canPrint(lvl) {
		return
	}
	l.lvlFormat(ctx, lvl, "%s", v...)
}

func newLvlWarp(lvl string) *LVLWrap {
	lvl = strings.ToLower(lvl)

	lo := new(LVLWrap)
	lo.w = bufio.NewWriter(os.Stdout)
	if l, ok := str2lvl[lvl]; ok {
		lo.l = l
	} else {
		lo.l = 2
	}

	return lo
}

func (l *LVLWrap) Fatal(errV error) {
	l.println(context.Background(), err, errV)
	os.Exit(1)
}

func newUDPLvlWarp(lvl string, addr *net.UDPAddr) (*LVLWrap, error) {
	lvl = strings.ToLower(lvl)

	lo := new(LVLWrap)
	lo.w = bufio.NewWriter(os.Stdout)
	if l, ok := str2lvl[lvl]; ok {
		lo.l = l
	} else {
		lo.l = 2
	}

	conn, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		return nil, err
	}

	lo.udpConn = conn

	return lo, nil
}
