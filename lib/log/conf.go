package log

type Conf struct {
	LVL     string `default:"info"`
	UDPAddr string `default:"0.0.0.0:12201" split_words:"true"`
}
