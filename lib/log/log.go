package log

import "net"

var Logger *LVLWrap

func MakeLogger(_lvl string) {
	Logger = newLvlWarp(_lvl)
}

func MakeUDPLogger(_lvl string, url string) error {
	var err error
	addr, err := net.ResolveUDPAddr("udp", url)
	if err != nil {
		return err
	}
	Logger, err = newUDPLvlWarp(_lvl, addr)
	return err
}
