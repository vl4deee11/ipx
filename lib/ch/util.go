package ch

import (
	"strconv"
	"strings"
)

func BuildLimitOffset(q string, l, o int) string {
	var sb strings.Builder
	sb.Grow(len(q) + 20)
	sb.WriteString(q)
	sb.WriteString(" limit ")
	sb.WriteString(strconv.Itoa(l))
	sb.WriteString(" offset ")
	sb.WriteString(strconv.Itoa(o))
	return sb.String()
}

func BuildLimitOffsetInt64(q string, l, o int64) string {
	var sb strings.Builder
	sb.Grow(len(q) + 20)
	sb.WriteString(q)
	sb.WriteString(" limit ")
	sb.WriteString(strconv.FormatInt(l, 10))
	sb.WriteString(" offset ")
	sb.WriteString(strconv.FormatInt(o, 10))
	return sb.String()
}
