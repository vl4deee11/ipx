package ch

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/log"

	// Register some standard stuff
	_ "github.com/ClickHouse/clickhouse-go"
)

func Conn(host, dbName, user, pass string, port uint16) (*sql.DB, error) {
	db, err := sql.Open(
		"clickhouse",
		fmt.Sprintf("tcp://%s:%d?database=%s&username=%s&password=%s", host, port, dbName, user, pass),
	)
	if err != nil {
		return nil, err
	}

	ctxBg := context.Background()
	pingTry := 0
	for {
		ctx, cancel := context.WithTimeout(ctxBg, timeoutPing)
		err = db.PingContext(ctx)
		cancel()
		if err != nil {
			log.Logger.Errorf(ctx, "ch: try to ping. ERR => %s\n", err.Error())
			if time.Duration(pingTry)*time.Second > 1*time.Minute {
				// Dont sleep more than minute
				pingTry = 0
			}
			time.Sleep(time.Duration(pingTry) * time.Second)
			pingTry++
			continue
		}
		break
	}
	log.Logger.Debug(ctxBg, "ch: connected")
	return db, nil
}
