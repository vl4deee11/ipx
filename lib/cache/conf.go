package cache

type Conf struct {
	Host string `required:"true"`
	Port uint16 `required:"true"`
	DBN  int    `required:"true"`
	TTL  int    `required:"true"`
}
