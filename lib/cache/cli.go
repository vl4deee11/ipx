package cache

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/log"

	"github.com/go-redis/redis/v8"
)

var Cli *Client

type Client struct {
	ttlInSec int
	redis    *redis.Client
}

func NewCacheCli(host string, port uint16, dbn, ttlInSec int) error {
	if host == "" {
		return errors.New("cache: host == <empty string>")
	}

	if ttlInSec == 0 {
		return errors.New("cache: ttl for cache == 0")
	}

	Cli = &Client{
		redis: redis.NewClient(&redis.Options{
			Addr: fmt.Sprintf("%s:%d", host, port),
			DB:   dbn,
		}),
		ttlInSec: ttlInSec,
	}
	log.Logger.Debug(context.Background(), "cache: connected")
	return nil
}

func (c *Client) Get(ctx context.Context, k string) (string, error) {
	cmd := c.redis.Get(ctx, k)
	if err := cmd.Err(); err != nil {
		return "", err
	}

	return cmd.Val(), nil
}

func (c *Client) Set(ctx context.Context, k, v string) error {
	return c.redis.Set(ctx, k, v, time.Duration(c.ttlInSec)*time.Second).Err()
}

func (c *Client) Expire(ctx context.Context, k string) error {
	return c.redis.Do(ctx, "EXPIRE", k, c.ttlInSec).Err()
}

func (c *Client) SetWithoutTTL(ctx context.Context, k, v string) error {
	return c.redis.Set(ctx, k, v, 0).Err()
}
