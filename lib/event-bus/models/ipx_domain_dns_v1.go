package models

import (
	"encoding/json"
	"time"

	"github.com/segmentio/kafka-go"
)

const TopicIpxDomainDNSV1 = "ipx.domain.dns.v1"

// IpxDomainDNSV1Event TOPIC: ipx.domain.dns.v1
type IpxDomainDNSV1Event struct {
	Domain string    `json:"domain"`
	IPs    []string  `json:"ips"`
	Time   time.Time `json:"time"`
}

func (e *IpxDomainDNSV1Event) ToMessage() kafka.Message {
	b, _ := json.Marshal(e)
	return kafka.Message{
		Key:   []byte(e.Domain),
		Value: b,
	}
}
