package eventbus

import (
	"context"
	"time"

	"github.com/segmentio/kafka-go"
	"gitlab.com/vl4deee11/ipx/lib/log"
)

type Model interface {
	ToMessage() kafka.Message
}

type ConsumerI interface {
	Consume(ctx context.Context)
}

type ConsumerHandlerI interface {
	Handle(b kafka.Message) bool
}

type Consumer struct {
	r       *kafka.Reader
	handler ConsumerHandlerI
}

func NewConsumer(brokers []string, servName, topic string, startOff int64, h ConsumerHandlerI) ConsumerI {
	return &Consumer{
		r: kafka.NewReader(kafka.ReaderConfig{
			Brokers:     brokers,
			Topic:       topic,
			GroupID:     servName + ":" + topic,
			StartOffset: startOff,
		}),
		handler: h,
	}
}

func (c *Consumer) Consume(ctx context.Context) {
	go c.consume(ctx)
}

const timeout = 60 * time.Second

func (c *Consumer) consume(ctx context.Context) {
	for {
		msg, err := c.r.FetchMessage(ctx)
		if err != nil {
			log.Logger.Errorf(ctx, "consumer: consume new ipsv6. ERR => %s\n", err.Error())
			continue
		}

		for !c.handler.Handle(msg) {
		}

		ctx2, cancel := context.WithTimeout(ctx, timeout)
		err = c.r.CommitMessages(ctx2, msg)
		if err != nil {
			cancel()
			log.Logger.Errorf(ctx, "consumer: commit msg err. ERR => %s\n", err.Error())
			continue
		}
		cancel()
	}
}
