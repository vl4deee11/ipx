package eventbus

import (
	"context"

	"github.com/segmentio/kafka-go"
)

type ProducerI interface {
	Produce(ctx context.Context, m Model) error
}

type Producer struct {
	w *kafka.Writer
}

func NewProducer(brokers []string, topic string, batchSize int) ProducerI {
	return &Producer{
		w: kafka.NewWriter(kafka.WriterConfig{
			Brokers:   brokers,
			Topic:     topic,
			BatchSize: batchSize,
			// Main broker accept: check https://www.sohamkamani.com/golang/working-with-kafka/#creating-the-kafka-consumer
			RequiredAcks: 1,
		}),
	}
}

func (p *Producer) Produce(ctx context.Context, m Model) error {
	return p.w.WriteMessages(ctx, m.ToMessage())
}
