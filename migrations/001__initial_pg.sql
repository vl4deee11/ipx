------------------------------------------------------------------------------------------------------------------------
------------------------------------------------log-x-inspector---------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE ctlog_process
(
    id        BIGSERIAL PRIMARY KEY NOT NULL,
    ctlog     TEXT                  NOT NULL,
    ct_limit  INT                   NOT NULL,
    ct_offset BIGINT                NOT NULL
);

CREATE
    UNIQUE INDEX ctlog_process_ctlog_uindex
    ON ctlog_process (ctlog);

CREATE
    UNIQUE INDEX ctlog_process_id_uindex
    ON ctlog_process (id);

ALTER TABLE ctlog_process
    ADD CONSTRAINT ctlog_process_pk
        PRIMARY KEY (id);

------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------log-ipv6-scan------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE ipsv6
(
    ipv6       VARCHAR(39) NOT NULL UNIQUE,
    handled_at TIMESTAMP WITH TIME ZONE
);

ALTER TABLE ipsv6
    ADD CONSTRAINT ipsv6_pk
        PRIMARY KEY (ipv6);

CREATE TABLE ipsv6_graph
(
    id         BIGSERIAL NOT NULL UNIQUE,
    parent_id  BIGSERIAL,
    net_prefix TEXT      NOT NULL UNIQUE,
    weight     BIGINT    NOT NULL
);

ALTER TABLE ipsv6_graph
    ADD CONSTRAINT ipsv6_graph_pk
        PRIMARY KEY (id);

ALTER TABLE ipsv6_graph
    ADD last_processed_at TIMESTAMP WITH TIME ZONE;

ALTER TABLE ipsv6
    ADD is_alive BOOLEAN DEFAULT TRUE;

ALTER TABLE ipsv6
    ADD locked_at TIMESTAMP WITH TIME ZONE;
