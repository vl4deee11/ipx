package pg

import "context"

type LogInfo struct {
	CTLog  string `db:"ctlog"`
	Limit  int32  `db:"ct_limit"`
	Offset int64  `db:"ct_offset"`
}

func (r *repository) GetCTLogByID(ctx context.Context, id int64) (*LogInfo, error) {
	var inf = new(LogInfo)
	err := r.db.GetContext(ctx, inf, selectInspectorLogByID, id)
	if err != nil {
		return nil, err
	}

	return inf, err
}

func (r *repository) UpdateCTLogByIDWithOffset(ctx context.Context, id, offset int64) error {
	_, err := r.db.ExecContext(ctx, updateInspectorLogByID, id, offset)
	return err
}
