package pg

import (
	"errors"

	"github.com/jmoiron/sqlx"
	"gitlab.com/vl4deee11/ipx/lib/pg"
)

type repository struct {
	db *sqlx.DB
}

var Repo *repository

func NewRepository(host, user, pass, dbName string, port uint16) error {
	if host == "" {
		return errors.New("pg: host == <empty string>")
	}

	if dbName == "" {
		return errors.New("pg: dbName == <empty string>")
	}

	Repo = &repository{}
	db, err := pg.Conn(host, user, pass, dbName, port)
	if err != nil {
		return err
	}
	Repo.db = db

	return nil
}
