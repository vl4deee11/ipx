package pg

const selectInspectorLogByID = `
SELECT ctlog, ct_limit, ct_offset
FROM ctlog_process
WHERE id = $1 LIMIT 1;`

const updateInspectorLogByID = `
UPDATE ctlog_process
SET ct_offset = $2
WHERE id = $1;`
