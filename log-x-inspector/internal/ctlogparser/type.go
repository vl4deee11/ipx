package ctlogparser

type EntriesResponse struct {
	Entries []struct {
		LeafInput []byte `json:"leaf_input"`
	} `json:"entries"`
}
