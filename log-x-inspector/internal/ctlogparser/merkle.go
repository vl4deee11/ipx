package ctlogparser

import (
	"errors"

	ct "github.com/google/certificate-transparency-go"
	"github.com/google/certificate-transparency-go/x509"
)

type MerkleTreeLeaf struct {
	Version          ct.Version        `tls:"maxval:255"`
	LeafType         ct.MerkleLeafType `tls:"maxval:255"`
	TimestampedEntry *timestampedEntry `tls:"selector:LeafType,val:0"`
}

type timestampedEntry struct {
	Timestamp uint64
	EntryType ct.LogEntryType `tls:"maxval:65535"`
	X509Entry *ct.ASN1Cert    `tls:"selector:EntryType,val:0"`
	PreCert   *ct.PreCert     `tls:"selector:EntryType,val:1"`
}

func (mtl *MerkleTreeLeaf) ToX509() (*x509.Certificate, error) {
	if mtl.TimestampedEntry.EntryType == ct.X509LogEntryType {
		return x509.ParseCertificate(mtl.TimestampedEntry.X509Entry.Data)
	} else if mtl.TimestampedEntry.EntryType == ct.PrecertLogEntryType {
		return x509.ParseTBSCertificate(mtl.TimestampedEntry.PreCert.TBSCertificate)
	}
	return nil, errors.New("type not found")
}
