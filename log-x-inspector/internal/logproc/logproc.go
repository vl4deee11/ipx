package logproc

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	eventbus "gitlab.com/vl4deee11/ipx/lib/event-bus"
	"gitlab.com/vl4deee11/ipx/lib/event-bus/models"

	"gitlab.com/vl4deee11/ipx/lib/dns"
	"gitlab.com/vl4deee11/ipx/lib/fpctx"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/log-x-inspector/internal/ctlogparser"
	"gitlab.com/vl4deee11/ipx/log-x-inspector/internal/pg"

	"github.com/google/certificate-transparency-go/tls"
)

type LogProc struct {
	ID          int64
	CTLog       string
	lim         int64
	off         int64
	buffSize    int
	httpCli     *http.Client
	concurrency int
	ch          chan *ctlogparser.EntriesResponse
	producer    eventbus.ProducerI
}

func NewProc(id int64, concurrency, buffSize int, brokers []string) (*LogProc, error) {
	info, err := pg.Repo.GetCTLogByID(context.Background(), id)
	if err != nil {
		return nil, fmt.Errorf("logproc: %s", err.Error())
	}

	if len(brokers) == 0 {
		return nil, errors.New("logproc: len(brokers) == 0")
	}

	return &LogProc{
		ch:          make(chan *ctlogparser.EntriesResponse, 1),
		ID:          id,
		CTLog:       info.CTLog + "/ct/v1/get-entries?start=%d&end=%d",
		lim:         int64(info.Limit),
		off:         info.Offset,
		concurrency: concurrency,
		buffSize:    buffSize,
		//nolint:gomnd // test check
		httpCli:  &http.Client{Timeout: time.Duration(15) * time.Second},
		producer: eventbus.NewProducer(brokers, models.TopicIpxDomainDNSV1, buffSize),
	}, nil
}

func (p *LogProc) Process() {
	for i := 0; i < p.concurrency; i++ {
		go p.handle()
	}

	go p.read()
}

func (p *LogProc) read() {
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)
	for {
		ctx, cancel = fpctx.WorkerCtx()
		next := p.off + p.lim
		log.Logger.Infof(ctx, "logproc: scan id=%d, start=%d -> end=%d", p.ID, p.off, next)
		req, _ := http.NewRequestWithContext(ctx, http.MethodGet, fmt.Sprintf(p.CTLog, p.off, next), nil)
		resp, err := p.httpCli.Do(req)
		if err != nil {
			cancel()
			log.Logger.Errorf(ctx, "logproc: cannot get log  with id=%d, offset=%d. ERR => %s\n", p.ID, p.off, err.Error())
			continue
		}

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			cancel()
			log.Logger.Errorf(ctx, "logproc: cannot read body id=%d. ERR => %s\n", p.ID, err.Error())
			continue
		}

		entries := new(ctlogparser.EntriesResponse)
		if err = json.Unmarshal(body, entries); err != nil {
			cancel()
			log.Logger.Errorf(ctx, "logproc: cannot marshall entries json id=%d. ERR => %s\n", p.ID, err.Error())
			_ = resp.Body.Close()
			continue
		}

		p.ch <- entries

		next++
		if err = pg.Repo.UpdateCTLogByIDWithOffset(ctx, p.ID, next); err != nil {
			cancel()
			log.Logger.Errorf(ctx, "logproc: cannot update log info id=%d. ERR => %s\n", p.ID, err.Error())
			_ = resp.Body.Close()
			continue
		}
		_ = resp.Body.Close()
		p.off = next
	}
}

func (p *LogProc) handle() {
	var (
		mtl  ctlogparser.MerkleTreeLeaf
		buff = make([]string, p.buffSize)
		bi   = 0
		ctx  context.Context
		err  error
	)
	for e := range p.ch {
		ctx = fpctx.WorkerCtxEndless()
		for i := range e.Entries {
			if _, err = tls.Unmarshal(e.Entries[i].LeafInput, &mtl); err != nil {
				log.Logger.Errorf(ctx, "logproc.handle: marshaling error. ERR => %s\n", err.Error())
				continue
			}

			ctCert, err := mtl.ToX509()
			if err != nil && !strings.Contains(err.Error(), "NonFatalErrors:") {
				log.Logger.Errorf(ctx, "logproc.handle: parsing error. ERR => %s\n", err.Error())
				continue
			}

			for di := range ctCert.DNSNames {
				buff[bi] = ctCert.DNSNames[di]
				bi++
				if bi >= p.buffSize {
					p.produceRetry(ctx, dns.Res.Resolve(ctx, buff))
					bi = 0
				}
			}
		}
	}
}

func (p *LogProc) produceRetry(ctx context.Context, domainsToIPs map[string][]string) {
	msgs := make([]*models.IpxDomainDNSV1Event, 0, len(domainsToIPs))
	now := time.Now()
	for dom := range domainsToIPs {
		msgs = append(msgs, &models.IpxDomainDNSV1Event{
			Domain: dom,
			IPs:    domainsToIPs[dom],
			Time:   now,
		})
	}

	i := 0
	for i < len(msgs) {
		if err := p.producer.Produce(ctx, msgs[i]); err != nil {
			log.Logger.Errorf(ctx, "logproc.handle: produce domains. ERR => %s\n", err.Error())
		} else {
			i++
		}
	}
}
