package main

import (
	"context"
	"fmt"
	"net/http"
	_ "time"

	eventbus "gitlab.com/vl4deee11/ipx/lib/event-bus"

	"gitlab.com/vl4deee11/ipx/lib/pg"

	"gitlab.com/vl4deee11/ipx/lib/dns"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	"gitlab.com/vl4deee11/ipx/log-x-inspector/internal/logproc"
	spg "gitlab.com/vl4deee11/ipx/log-x-inspector/internal/pg"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Log log.Conf
	EB  eventbus.Conf
	PG  pg.Conf
	DNS dns.Conf

	LogProc struct {
		IDs         []int64 `required:"true"`
		Concurrency int     `required:"true"`
		BuffSize    int     `split_words:"true" required:"true"`
	} `split_words:"true" required:"true"`

	HTTP struct {
		Port uint16 `required:"true"`
	}
}

func main() {
	c, err := initialize()
	if err != nil {
		panic(err)
	}

	log.MakeLogger(c.Log.LVL)
	fmt.Println("main: service is start v=3.0.0")

	if err = dns.NewResolver(c.DNS.Addr); err != nil {
		log.Logger.Fatal(err)
		return
	}

	if err = spg.NewRepository(c.PG.Host, c.PG.User, c.PG.Pass, c.PG.DBName, c.PG.Port); err != nil {
		log.Logger.Fatal(err)
		return
	}

	var lrs = make([]*logproc.LogProc, len(c.LogProc.IDs))
	for i := range c.LogProc.IDs {
		lrs[i], err = logproc.NewProc(c.LogProc.IDs[i], c.LogProc.Concurrency, c.LogProc.BuffSize, c.EB.Brokers)
		if err != nil {
			log.Logger.Fatal(err)
			return
		}
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", xhttp.MSRoute)
	mux.Handle("/metrics", promhttp.Handler())

	// Add the pprof routes
	// mux.HandleFunc("/debug/pprof/", pprof.Index)
	// mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	// mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	// mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	// mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	//
	// mux.Handle("/debug/pprof/block", pprof.Handler("block"))
	// mux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	// mux.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	// mux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	for i := range lrs {
		lrs[i].Process()
	}
	log.Logger.Info(context.Background(), "main: service is up")
	log.Logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", c.HTTP.Port), mux))
}

func initialize() (*Config, error) {
	c := &Config{}
	err := envconfig.Process("log_x_inspector", c)
	if err != nil {
		return nil, err
	}
	return c, nil
}
