export LOG_DWH_DOMAIN_DNS_CONSUMER_WAIT_TIME=5s


export LOG_DWH_LOG_LVL=debug

export LOG_DWH_EB_BROKERS=0.0.0.0:9092


export LOG_DWH_CH_DB_NAME=s11
export LOG_DWH_CH_USER=s11
export LOG_DWH_CH_PASS=s11
export LOG_DWH_CH_PORT=9000
export LOG_DWH_CH_HOST=0.0.0.0

export LOG_DWH_HTTP_PORT=8081
export GODEBUG=madvdontneed=1

go run ./cmd/main.go
