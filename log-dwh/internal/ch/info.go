package ch

import (
	"context"
	"time"

	"github.com/ClickHouse/clickhouse-go"
)

func (r *repository) InsertIps(ctx context.Context, ips []string, domain string, ts time.Time) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.PrepareContext(ctx, insertIPs)
	if err != nil {
		return err
	}

	defer stmt.Close()

	if _, err = stmt.ExecContext(ctx, domain, clickhouse.Array(ips), ts); err != nil {
		return err
	}

	if err = tx.Commit(); err != nil {
		return err
	}
	return nil
}
