package domaindns

import (
	"context"
	"encoding/json"
	"time"

	"github.com/segmentio/kafka-go"
	eb "gitlab.com/vl4deee11/ipx/lib/event-bus"
	"gitlab.com/vl4deee11/ipx/lib/event-bus/models"
	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/log-dwh/internal/ch"
)

const timeout = 60 * time.Second

type Handler struct {
	waitTime time.Duration
}

func NewConsumer(brokers []string, servName string, waitTime time.Duration) eb.ConsumerI {
	return eb.NewConsumer(brokers, servName, models.TopicIpxDomainDNSV1, kafka.FirstOffset, &Handler{waitTime: waitTime})
}

func (c *Handler) Handle(msg kafka.Message) bool {
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	model := &models.IpxDomainDNSV1Event{}

	err = json.Unmarshal(msg.Value, model)
	if err != nil {
		log.Logger.Errorf(ctx, "domain_dns_consumer: json unmarshall %s. ERR => %s\n", string(msg.Value), err.Error())
		time.Sleep(c.waitTime)
		return true
	}

	err = ch.Repo.InsertIps(ctx, model.IPs, model.Domain, model.Time)
	if err != nil {
		log.Logger.Errorf(ctx, "domain_dns_consumer: domain dns insert. ERR => %s\n", err.Error())
		time.Sleep(c.waitTime)
		return false
	}

	log.Logger.Info(ctx, "domain_dns_consumer: domain dns insert new")
	return true
}
