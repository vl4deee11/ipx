package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/vl4deee11/ipx/lib/ch"
	domaindns "gitlab.com/vl4deee11/ipx/log-dwh/internal/event/domain_dns"

	eventbus "gitlab.com/vl4deee11/ipx/lib/event-bus"

	"gitlab.com/vl4deee11/ipx/lib/log"
	"gitlab.com/vl4deee11/ipx/lib/xhttp"
	sch "gitlab.com/vl4deee11/ipx/log-dwh/internal/ch"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/kelseyhightower/envconfig"
)

const (
	serviceName = "log_dwh"
)

type Config struct {
	Log log.Conf
	EB  eventbus.Conf
	CH  ch.Conf

	HTTP struct {
		Port uint16 `required:"true"`
	}

	DomainDnsConsumer struct {
		WaitTime time.Duration `required:"true" split_words:"true"`
	} `split_words:"true" required:"true"`
}

func main() {
	c, err := initialize()
	if err != nil {
		panic(err)
	}

	log.MakeLogger(c.Log.LVL)
	fmt.Println("main: service is start v=3.0.0")

	if err = sch.NewRepository(c.CH.Host, c.CH.User, c.CH.Pass, c.CH.DBName, c.CH.Port); err != nil {
		log.Logger.Fatal(err)
		return
	}

	domainDnsCons := domaindns.NewConsumer(c.EB.Brokers, serviceName, c.DomainDnsConsumer.WaitTime)

	mux := http.NewServeMux()
	mux.HandleFunc("/", xhttp.MSRoute)
	mux.Handle("/metrics", promhttp.Handler())

	// Add the pprof routes
	// mux.HandleFunc("/debug/pprof/", pprof.Index)
	// mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	// mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	// mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	// mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	//
	// mux.Handle("/debug/pprof/block", pprof.Handler("block"))
	// mux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	// mux.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	// mux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	domainDnsCons.Consume(context.Background())
	log.Logger.Info(context.Background(), "main: service is up")
	log.Logger.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", c.HTTP.Port), mux))
}

func initialize() (*Config, error) {
	c := &Config{}
	err := envconfig.Process(serviceName, c)
	if err != nil {
		return nil, err
	}
	return c, nil
}
